package cc.altruix.dschallenge.task2;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.BufferedMutator;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.hbase.util.Pair;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.log4j.BasicConfigurator;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka010.ConsumerStrategies;
import org.apache.spark.streaming.kafka010.KafkaUtils;
import org.apache.spark.streaming.kafka010.LocationStrategies;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cc.altruix.dschallenge.commons.IConfigValidator;
import cc.altruix.dschallenge.commons.Utils;
import cc.altruix.dschallenge.commons.ValidationResult;

public class App {
    private final static String CONFIG_FILE_NAME = "task2.config.properties";
    public static final String TABLE_NAME = "signals";
    private final IConfigValidator configValidator;
    public static final byte[] SIGNALS_FAMILY = Bytes.toBytes("signalsFamily");
    public static final long INVALID_LONG = -1L;
    private final Logger logger;

    public App(final IConfigValidator configValidator, final Logger logger) {
        this.configValidator = configValidator;
        this.logger = logger;
    }

    public App() {
        this(new ConfigValidator(), LoggerFactory.getLogger(App.class));
    }

    public void run() {
        final Properties config = readConfig();
        final ValidationResult valRes = configValidator.validate(config);
        if (!valRes.isValid()) {
            logger.error(String
                            .format("There is a problem with the configuration file: %s",
                                            valRes.getMessage()));
            return;
        }
        start(config);
    }

    protected Properties readConfig() {
        return Utils.readConfig(CONFIG_FILE_NAME, logger);
    }

    void start(final Properties cfg) {
        configureLog4j(); // HBase logs via log4j
        final Configuration hbaseCfg = createHbaseConfig(cfg);
        final Map<String, Object> kafkaParams = createKafkaParams(cfg);
        final Collection<String> topics = composeTopicsList(cfg);
        final SparkConf conf = createSparkConf();
        final ObjectMapper objectMapper = createObjectMapper();
        Connection hbaseConn = null;
        JavaStreamingContext sparkCtx = null;
        BufferedMutator table = null;
        try {
            hbaseConn = createConnection(hbaseCfg);
            sparkCtx = createSpakStreamingContext(conf);
            table = getTable(hbaseConn, TABLE_NAME);
            final JavaInputDStream<ConsumerRecord<String, String>> stream = createStream(
                            kafkaParams, topics, sparkCtx);
            final BufferedMutator targetTable = table;
            stream.foreachRDD(
                            rdd -> processRdd(objectMapper, targetTable, rdd));
            sparkCtx.start();
            sparkCtx.awaitTermination();
        } catch (final IOException exception) {
            logger.error("", exception);
        } catch (final RuntimeException exception) {
            logger.error("", exception);
        } catch (final InterruptedException exception) {
            logger.error("", exception);
        } finally {
            closeTable(table);
            closeHBase(hbaseConn);
        }
    }

    Collection<String> composeTopicsList(final Properties cfg) {
        return Arrays.asList(cfg.getProperty("srcTopic"));
    }

    void processRdd(final ObjectMapper objectMapper,
                    final BufferedMutator targetTable,
                    final JavaRDD<ConsumerRecord<String, String>> rdd) {
        final List<ConsumerRecord<String, String>> rdds = rdd.collect();
        rdds.stream().map(x -> rddToJson(x, objectMapper)).map(this::jsonToPut)
                        .forEach(put -> saveInHbase(put, targetTable));
    }

    JavaInputDStream<ConsumerRecord<String, String>> createStream(
                    final Map<String, Object> kafkaParams,
                    final Collection<String> topics,
                    final JavaStreamingContext sparkCtx) {
        return KafkaUtils.createDirectStream(sparkCtx,
                        LocationStrategies.PreferConsistent(),
                        ConsumerStrategies.Subscribe(topics, kafkaParams));
    }

    BufferedMutator getTable(final Connection hbaseConn, final String tableName)
                    throws IOException {
        return hbaseConn.getBufferedMutator(TableName.valueOf(tableName));
    }

    JavaStreamingContext createSpakStreamingContext(SparkConf conf) {
        return new JavaStreamingContext(conf, Durations.seconds(1));
    }

    Connection createConnection(Configuration hbaseCfg) throws IOException {
        return ConnectionFactory.createConnection(hbaseCfg);
    }

    ObjectMapper createObjectMapper() {
        return new ObjectMapper();
    }

    void configureLog4j() {
        BasicConfigurator.configure();
    }

    SparkConf createSparkConf() {
        return new SparkConf().setMaster("local[2]").setAppName("Task2")
                        .set("spark.serializer",
                                        "org.apache.spark.serializer.KryoSerializer");
    }

    Map<String, Object> createKafkaParams(final Properties cfg) {
        final Map<String, Object> kafkaParams = new HashMap<>();
        kafkaParams.put("bootstrap.servers",
                        cfg.getProperty("bootstrap.servers"));
        kafkaParams.put("key.deserializer",
                        org.apache.kafka.common.serialization.StringDeserializer.class);
        kafkaParams.put("value.deserializer",
                        org.apache.kafka.common.serialization.StringDeserializer.class);
        kafkaParams.put("group.id", "group1");
        kafkaParams.put("auto.offset.reset", "latest");
        kafkaParams.put("enable.auto.commit", false);
        return kafkaParams;
    }

    Configuration createHbaseConfig(final Properties cfg) {
        final Configuration hbaseCfg = HBaseConfiguration.create();
        hbaseCfg.set("hbase.master", cfg.getProperty("hbase.master"));
        hbaseCfg.set("hbase.zookeeper.quorum",
                        cfg.getProperty("hbase.zookeeper.quorum"));
        hbaseCfg.set("hbase.zookeeper.property.clientPort",
                        cfg.getProperty("hbase.zookeeper.property.clientPort"));
        return hbaseCfg;
    }

    protected void closeTable(final BufferedMutator table) {
        if (table != null) {
            try {
                table.close();
            } catch (final IOException exception) {
                logger.error("", exception);
            }
        }
    }

    protected void saveInHbase(final Put put,
                    final BufferedMutator targetTable) {
        try {
            targetTable.mutate(put);
            targetTable.flush();
        } catch (final IOException exception) {
            logger.error("", exception);
        }
    }

    protected Put jsonToPut(final Pair<String, JsonNode> input) {
        final String rawText = input.getFirst();
        final JsonNode json = input.getSecond();
        final String rowId = generateRowId();
        final Put record = new Put(Bytes.toBytes(rowId));
        record.addColumn(SIGNALS_FAMILY, Bytes.toBytes("raw"),
                        rawText.getBytes());
        if (json != null) {
            final String deviceId = nullSafeExtractString(json, "deviceId");
            final String temperatureText = nullSafeExtractString(json,
                            "temperature");
            final String lat = nullSafeExtractString(json.get("location"),
                            "latitude");
            final String lon = nullSafeExtractString(json.get("location"),
                            "longitude");
            final long timeMillis = nullSafeExtractLong(json, "time");
            final String dateString = extractDateString(timeMillis);

            record.addColumn(SIGNALS_FAMILY, Bytes.toBytes("deviceId"),
                            deviceId.getBytes());
            record.addColumn(SIGNALS_FAMILY, Bytes.toBytes("temperature"),
                            temperatureText.getBytes());
            record.addColumn(SIGNALS_FAMILY, Bytes.toBytes("lat"),
                            lat.getBytes());
            record.addColumn(SIGNALS_FAMILY, Bytes.toBytes("lon"),
                            lon.getBytes());
            record.addColumn(SIGNALS_FAMILY, Bytes.toBytes("time"),
                            dateString.getBytes());
        }
        return record;
    }

    String generateRowId() {
        return UUID.randomUUID().toString();
    }

    String extractDateString(final long timeMillis) {
        final String dateString;

        if (timeMillis == INVALID_LONG) {
            dateString = "";
        } else {
            final Date date = new Date(timeMillis);
            final SimpleDateFormat format = new SimpleDateFormat(
                            "yyyy-MM-dd'T'HH:mm:ssXXX");
            dateString = format.format(date);
        }
        return dateString;
    }

    protected long nullSafeExtractLong(final JsonNode json,
                    final String nodeName) {
        if (json == null) {
            return INVALID_LONG;
        }
        final JsonNode node = json.get(nodeName);
        if (node == null) {
            return INVALID_LONG;
        }
        return node.asLong();
    }

    protected String nullSafeExtractString(final JsonNode json,
                    final String nodeName) {
        if (json == null) {
            return "";
        }
        final JsonNode node = json.get(nodeName);
        if (node == null) {
            return "";
        }
        return node.asText();
    }

    protected Pair<String, JsonNode> rddToJson(
                    final ConsumerRecord<String, String> rdd,
                    final ObjectMapper objectMapper) {
        try {
            final String valueTxt = rdd.value();
            final JsonNode jsonNode = objectMapper.readTree(valueTxt);
            return new Pair<>(valueTxt, jsonNode);
        } catch (final JsonProcessingException exception) {
            logger.error("", exception);
            return null;
        } catch (final IOException exception) {
            logger.error("", exception);
            return null;
        }
    }

    void closeHBase(final Connection hbaseConn) {
        if (hbaseConn != null) {
            try {
                hbaseConn.close();
            } catch (final IOException exception) {
                logger.error("", exception);
            }
        }
    }

    public static void main(final String[] args) throws InterruptedException {
        final App app = new App();
        app.run();
    }
}
