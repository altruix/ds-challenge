package cc.altruix.dschallenge.task2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import cc.altruix.dschallenge.commons.AbstractValidator;

public class ConfigValidator extends AbstractValidator {
    @Override
    protected List<String> getNonEmptyProps() {
        return Arrays.asList("hbase.master",
                        "hbase.zookeeper.quorum",
                        "hbase.zookeeper.property.clientPort",
                        "bootstrap.servers",
                        "group.id",
                        "auto.offset.reset",
                        "srcTopic");
    }

    @Override
    protected List<String> getNumericProps() {
        return Collections.singletonList("hbase.zookeeper.property.clientPort");
    }

    @Override
    protected List<String> getDateTimeProps() {
        return new ArrayList<>(0);
    }

    @Override
    protected List<Pair<String, String>> getMinMaxPairs() {
        return new ArrayList<>(0);
    }
}
