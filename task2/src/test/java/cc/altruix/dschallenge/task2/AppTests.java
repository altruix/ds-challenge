package cc.altruix.dschallenge.task2;

import java.io.IOException;
import java.util.AbstractMap;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Properties;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.client.BufferedMutator;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.hbase.util.Pair;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.fest.assertions.data.MapEntry;
import org.slf4j.Logger;

import cc.altruix.dschallenge.commons.IConfigValidator;
import cc.altruix.dschallenge.commons.ValidationResult;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class AppTests {
    private final static String SAMPLE_JSON = "{\"deviceId\":\"11c1310e-c0c2-461b-a4eb-f6bf8da2d23c\",\"temperature\":12,\"location\":{\"latitude\":\"10.0\",\"longitude\":\"20.0\"},\"time\":\"9459\"}";

    @Test
    public void startNoErrors() throws IOException, InterruptedException {
        // Prepare
        final IConfigValidator configValidator = mock(IConfigValidator.class);
        final Properties cfg = mock(Properties.class);
        final Logger logger = mock(Logger.class);
        final App sut = spy(new App(configValidator, logger));
        doNothing().when(sut).configureLog4j();
        final Configuration hbaseCfg = mock(Configuration.class);
        doReturn(hbaseCfg).when(sut).createHbaseConfig(cfg);
        final Map<String, Object> kafkaParams = mock(Map.class);
        doReturn(kafkaParams).when(sut).createKafkaParams(cfg);
        final Collection<String> topics = mock(Collection.class);
        doReturn(topics).when(sut).composeTopicsList(cfg);
        final SparkConf conf = mock(SparkConf.class);
        doReturn(conf).when(sut).createSparkConf();
        final ObjectMapper objectMapper = mock(ObjectMapper.class);
        doReturn(objectMapper).when(sut).createObjectMapper();
        final Connection hbaseConn = mock(Connection.class);
        doReturn(hbaseConn).when(sut).createConnection(hbaseCfg);
        final JavaStreamingContext sparkCtx = mock(JavaStreamingContext.class);
        doReturn(sparkCtx).when(sut).createSpakStreamingContext(conf);
        final BufferedMutator table = mock(BufferedMutator.class);
        doReturn(table).when(sut).getTable(hbaseConn, App.TABLE_NAME);
        final JavaInputDStream<ConsumerRecord<String, String>> stream = mock(
                        JavaInputDStream.class);
        doReturn(stream).when(sut).createStream(kafkaParams, topics, sparkCtx);
        doNothing().when(sut).closeTable(table);
        doNothing().when(sut).closeHBase(hbaseConn);

        // Run method under test
        sut.start(cfg);

        // Verify
        verify(sut).configureLog4j();
        verify(sut).createHbaseConfig(cfg);
        verify(sut).createKafkaParams(cfg);
        verify(sut).composeTopicsList(cfg);
        verify(sut).createSparkConf();
        verify(sut).createObjectMapper();
        verify(sut).createConnection(hbaseCfg);
        verify(sut).createSpakStreamingContext(conf);
        verify(sut).getTable(hbaseConn, App.TABLE_NAME);
        verify(sut).createStream(kafkaParams, topics, sparkCtx);
        verify(stream).foreachRDD(any(VoidFunction.class));
        verify(sparkCtx).start();
        verify(sparkCtx).awaitTermination();
        verify(sut).closeTable(table);
        verify(sut).closeHBase(hbaseConn);
    }

    @Test
    public void startErrorInterruptedException()
                    throws IOException, InterruptedException {
        // Prepare
        final IConfigValidator configValidator = mock(IConfigValidator.class);
        final Properties cfg = mock(Properties.class);
        final Logger logger = mock(Logger.class);
        final App sut = spy(new App(configValidator, logger));
        doNothing().when(sut).configureLog4j();
        final Configuration hbaseCfg = mock(Configuration.class);
        doReturn(hbaseCfg).when(sut).createHbaseConfig(cfg);
        final Map<String, Object> kafkaParams = mock(Map.class);
        doReturn(kafkaParams).when(sut).createKafkaParams(cfg);
        final Collection<String> topics = mock(Collection.class);
        doReturn(topics).when(sut).composeTopicsList(cfg);
        final SparkConf conf = mock(SparkConf.class);
        doReturn(conf).when(sut).createSparkConf();
        final ObjectMapper objectMapper = mock(ObjectMapper.class);
        doReturn(objectMapper).when(sut).createObjectMapper();
        final Connection hbaseConn = mock(Connection.class);
        doReturn(hbaseConn).when(sut).createConnection(hbaseCfg);
        final JavaStreamingContext sparkCtx = mock(JavaStreamingContext.class);
        doReturn(sparkCtx).when(sut).createSpakStreamingContext(conf);
        final BufferedMutator table = mock(BufferedMutator.class);
        doReturn(table).when(sut).getTable(hbaseConn, App.TABLE_NAME);
        final JavaInputDStream<ConsumerRecord<String, String>> stream = mock(
                        JavaInputDStream.class);
        doReturn(stream).when(sut).createStream(kafkaParams, topics, sparkCtx);
        doNothing().when(sut).closeTable(table);
        doNothing().when(sut).closeHBase(hbaseConn);
        final InterruptedException exception = new InterruptedException();
        doThrow(exception).when(sparkCtx).awaitTermination();

        // Run method under test
        sut.start(cfg);

        // Verify
        verify(sut).configureLog4j();
        verify(sut).createHbaseConfig(cfg);
        verify(sut).createKafkaParams(cfg);
        verify(sut).composeTopicsList(cfg);
        verify(sut).createSparkConf();
        verify(sut).createObjectMapper();
        verify(sut).createConnection(hbaseCfg);
        verify(sut).createSpakStreamingContext(conf);
        verify(sut).getTable(hbaseConn, App.TABLE_NAME);
        verify(sut).createStream(kafkaParams, topics, sparkCtx);
        verify(stream).foreachRDD(any(VoidFunction.class));
        verify(sparkCtx).start();
        verify(sparkCtx).awaitTermination();
        verify(logger).error("", exception);
        verify(sut).closeTable(table);
        verify(sut).closeHBase(hbaseConn);

    }

    @Test
    public void startErrorRuntimeException()
                    throws IOException, InterruptedException {
        // Prepare
        final IConfigValidator configValidator = mock(IConfigValidator.class);
        final Properties cfg = mock(Properties.class);
        final Logger logger = mock(Logger.class);
        final App sut = spy(new App(configValidator, logger));
        doNothing().when(sut).configureLog4j();
        final Configuration hbaseCfg = mock(Configuration.class);
        doReturn(hbaseCfg).when(sut).createHbaseConfig(cfg);
        final Map<String, Object> kafkaParams = mock(Map.class);
        doReturn(kafkaParams).when(sut).createKafkaParams(cfg);
        final Collection<String> topics = mock(Collection.class);
        doReturn(topics).when(sut).composeTopicsList(cfg);
        final SparkConf conf = mock(SparkConf.class);
        doReturn(conf).when(sut).createSparkConf();
        final ObjectMapper objectMapper = mock(ObjectMapper.class);
        doReturn(objectMapper).when(sut).createObjectMapper();
        final Connection hbaseConn = mock(Connection.class);
        doReturn(hbaseConn).when(sut).createConnection(hbaseCfg);
        final JavaStreamingContext sparkCtx = mock(JavaStreamingContext.class);
        doReturn(sparkCtx).when(sut).createSpakStreamingContext(conf);
        final BufferedMutator table = mock(BufferedMutator.class);
        doReturn(table).when(sut).getTable(hbaseConn, App.TABLE_NAME);
        final JavaInputDStream<ConsumerRecord<String, String>> stream = mock(
                        JavaInputDStream.class);
        doReturn(stream).when(sut).createStream(kafkaParams, topics, sparkCtx);
        doNothing().when(sut).closeTable(table);
        doNothing().when(sut).closeHBase(hbaseConn);
        final RuntimeException exception = new RuntimeException();
        doThrow(exception).when(sparkCtx).start();

        // Run method under test
        sut.start(cfg);

        // Verify
        verify(sut).configureLog4j();
        verify(sut).createHbaseConfig(cfg);
        verify(sut).createKafkaParams(cfg);
        verify(sut).composeTopicsList(cfg);
        verify(sut).createSparkConf();
        verify(sut).createObjectMapper();
        verify(sut).createConnection(hbaseCfg);
        verify(sut).createSpakStreamingContext(conf);
        verify(sut).getTable(hbaseConn, App.TABLE_NAME);
        verify(sut).createStream(kafkaParams, topics, sparkCtx);
        verify(stream).foreachRDD(any(VoidFunction.class));
        verify(sparkCtx).start();
        verify(sparkCtx, never()).awaitTermination();
        verify(logger).error("", exception);
        verify(sut).closeTable(table);
        verify(sut).closeHBase(hbaseConn);

    }

    @Test
    public void startErrorIOException() throws IOException {
        // Prepare
        final IOException exception = new IOException();
        final IConfigValidator configValidator = mock(IConfigValidator.class);
        final Properties cfg = mock(Properties.class);
        final Logger logger = mock(Logger.class);
        final App sut = spy(new App(configValidator, logger));
        doNothing().when(sut).configureLog4j();
        final Configuration hbaseCfg = mock(Configuration.class);
        doReturn(hbaseCfg).when(sut).createHbaseConfig(cfg);
        final Map<String, Object> kafkaParams = mock(Map.class);
        doReturn(kafkaParams).when(sut).createKafkaParams(cfg);
        final Collection<String> topics = mock(Collection.class);
        doReturn(topics).when(sut).composeTopicsList(cfg);
        final SparkConf conf = mock(SparkConf.class);
        doReturn(conf).when(sut).createSparkConf();
        final ObjectMapper objectMapper = mock(ObjectMapper.class);
        doReturn(objectMapper).when(sut).createObjectMapper();
        final Connection hbaseConn = mock(Connection.class);
        doReturn(hbaseConn).when(sut).createConnection(hbaseCfg);
        doThrow(exception).when(sut).createConnection(hbaseCfg);
        final JavaStreamingContext sparkCtx = mock(JavaStreamingContext.class);
        doReturn(sparkCtx).when(sut).createSpakStreamingContext(conf);
        final BufferedMutator table = mock(BufferedMutator.class);
        doReturn(table).when(sut).getTable(hbaseConn, App.TABLE_NAME);
        final JavaInputDStream<ConsumerRecord<String, String>> stream = mock(
                        JavaInputDStream.class);
        doReturn(stream).when(sut).createStream(kafkaParams, topics, sparkCtx);
        doNothing().when(sut).closeTable(table);
        doNothing().when(sut).closeHBase(hbaseConn);

        // Run method under test
        sut.start(cfg);

        // Verify
        verify(sut).configureLog4j();
        verify(sut).createHbaseConfig(cfg);
        verify(sut).createKafkaParams(cfg);
        verify(sut).composeTopicsList(cfg);
        verify(sut).createSparkConf();
        verify(sut).createObjectMapper();
        verify(sut).createConnection(hbaseCfg);
        verify(logger).error("", exception);
        verify(sut).closeTable(null);
        verify(sut).closeHBase(null);
    }

    @Test
    public void runConfigError() {
        // Prepare
        final IConfigValidator configValidator = mock(IConfigValidator.class);
        final Properties cfg = mock(Properties.class);
        final Logger logger = mock(Logger.class);
        final App sut = spy(new App(configValidator, logger));
        doReturn(cfg).when(sut).readConfig();
        final ValidationResult valRes = new ValidationResult(false, "msg");
        when(configValidator.validate(cfg)).thenReturn(valRes);

        // Run method under test
        sut.run();

        // Verify
        verify(sut).readConfig();
        verify(configValidator).validate(cfg);
        verify(logger).error(
                        "There is a problem with the configuration file: msg");
        verify(sut, never()).start(cfg);
    }

    @Test
    public void runConfigNoError() {
        // Prepare
        final IConfigValidator configValidator = mock(IConfigValidator.class);
        final Properties cfg = mock(Properties.class);
        final Logger logger = mock(Logger.class);
        final App sut = spy(new App(configValidator, logger));
        doReturn(cfg).when(sut).readConfig();
        final ValidationResult valRes = new ValidationResult(true, "");
        when(configValidator.validate(cfg)).thenReturn(valRes);
        doNothing().when(sut).start(cfg);

        // Run method under test
        sut.run();

        // Verify
        verify(sut).readConfig();
        verify(configValidator).validate(cfg);
        verify(logger, never()).error(anyString());
        verify(sut).start(cfg);
    }

    @Test
    public void closeTableClosesTable() throws IOException {
        // Prepare
        final IConfigValidator configValidator = mock(IConfigValidator.class);
        final Properties cfg = mock(Properties.class);
        final Logger logger = mock(Logger.class);
        final App sut = spy(new App(configValidator, logger));
        final BufferedMutator table = mock(BufferedMutator.class);

        // Run method under test
        sut.closeTable(table);

        // Verify
        verify(table).close();
    }

    @Test
    public void closeTableDoesNotThrowNpe() throws IOException {
        // Prepare
        final IConfigValidator configValidator = mock(IConfigValidator.class);
        final Logger logger = mock(Logger.class);
        final App sut = spy(new App(configValidator, logger));

        // Run method under test
        sut.closeTable(null);
    }

    @Test
    public void closeHBaseClosesConnection() throws IOException {
        // Prepare
        final IConfigValidator configValidator = mock(IConfigValidator.class);
        final Logger logger = mock(Logger.class);
        final App sut = spy(new App(configValidator, logger));
        final Connection hbaseConn = mock(Connection.class);

        // Run method under test
        sut.closeHBase(hbaseConn);

        // Verify
        verify(hbaseConn).close();
    }

    @Test
    public void closeHBaseDoesNotThrowNpe() throws IOException {
        // Prepare
        final IConfigValidator configValidator = mock(IConfigValidator.class);
        final Logger logger = mock(Logger.class);
        final App sut = spy(new App(configValidator, logger));

        // Run method under test
        sut.closeHBase(null);
    }

    @Test
    public void processRdd() {
        // Prepare
        final IConfigValidator configValidator = mock(IConfigValidator.class);
        final Logger logger = mock(Logger.class);
        final App sut = spy(new App(configValidator, logger));
        final ObjectMapper objectMapper = mock(ObjectMapper.class);
        final BufferedMutator targetTable = mock(BufferedMutator.class);
        final JavaRDD<ConsumerRecord<String, String>> rdd = mock(JavaRDD.class);
        final ConsumerRecord<String, String> properRdd = new ConsumerRecord<>(
                        "topic", 1, 0L, "", "");
        final List<ConsumerRecord<String, String>> rdds = Collections
                        .singletonList(properRdd);
        when(rdd.collect()).thenReturn(rdds);
        final Pair<String, JsonNode> json = mock(Pair.class);
        doReturn(json).when(sut).rddToJson(properRdd, objectMapper);
        final Put put = mock(Put.class);
        doReturn(put).when(sut).jsonToPut(json);
        doNothing().when(sut).saveInHbase(put, targetTable);

        // Run method under test
        sut.processRdd(objectMapper, targetTable, rdd);

        // Verify
        verify(sut).rddToJson(properRdd, objectMapper);
        verify(sut).jsonToPut(json);
        verify(sut).saveInHbase(put, targetTable);
    }

    @Test
    public void createKafkaParams() {
        // Prepare
        final IConfigValidator configValidator = mock(IConfigValidator.class);
        final Logger logger = mock(Logger.class);
        final App sut = new App(configValidator, logger);
        final Properties cfg = new Properties();
        cfg.setProperty("bootstrap.servers", "bootstrap.servers.value");

        // Run method under test
        final Map<String, Object> actRes = sut.createKafkaParams(cfg);

        // Verify
        assertThat(actRes).isNotNull().contains(MapEntry
                                        .entry("bootstrap.servers", "bootstrap.servers.value"),
                        MapEntry.entry("key.deserializer",
                                        org.apache.kafka.common.serialization.StringDeserializer.class),
                        MapEntry.entry("value.deserializer",
                                        org.apache.kafka.common.serialization.StringDeserializer.class),
                        MapEntry.entry("group.id", "group1"),
                        MapEntry.entry("auto.offset.reset", "latest"),
                        MapEntry.entry("enable.auto.commit", false));
    }

    @Test
    public void createHbaseConfig() {
        // Prepare
        final IConfigValidator configValidator = mock(IConfigValidator.class);
        final Logger logger = mock(Logger.class);
        final App sut = new App(configValidator, logger);
        final Properties cfg = new Properties();
        cfg.setProperty("hbase.master", "hbase.master.value");
        cfg.setProperty("hbase.zookeeper.quorum",
                        "hbase.zookeeper.quorum.value");
        cfg.setProperty("hbase.zookeeper.property.clientPort",
                        "hbase.zookeeper.property.clientPort.value");

        // Run method under test
        final Configuration actRes = sut.createHbaseConfig(cfg);

        // Verify
        assertThat(actRes).isNotNull()
                        .contains(new AbstractMap.SimpleEntry("hbase.master",
                                                        "hbase.master.value"),
                                        new AbstractMap.SimpleEntry(
                                                        "hbase.zookeeper.quorum",
                                                        "hbase.zookeeper.quorum.value"),
                                        new AbstractMap.SimpleEntry(
                                                        "hbase.zookeeper.property.clientPort",
                                                        "hbase.zookeeper.property.clientPort.value"));
    }

    @Test
    public void saveInHbase() throws IOException {
        // Prepare
        final IConfigValidator configValidator = mock(IConfigValidator.class);
        final Logger logger = mock(Logger.class);
        final App sut = new App(configValidator, logger);
        final Put put = mock(Put.class);
        final BufferedMutator targetTable = mock(BufferedMutator.class);

        // Run method under test
        sut.saveInHbase(put, targetTable);

        // Verify
        verify(targetTable).mutate(put);
        verify(targetTable).flush();
    }

    @Test
    public void jsonToPutNoJson() {
        // Prepare
        final IConfigValidator configValidator = mock(IConfigValidator.class);
        final Logger logger = mock(Logger.class);
        final App sut = spy(new App(configValidator, logger));
        final Pair<String, JsonNode> input = Pair.newPair("txt", null);
        final String rowId = "rowId";
        doReturn(rowId).when(sut).generateRowId();

        // Run method under test
        final Put actRes = sut.jsonToPut(input);

        // Verify
        assertThat(actRes.getRow()).isEqualTo(Bytes.toBytes(rowId));
        final NavigableMap<byte[], List<Cell>> x = actRes.getFamilyCellMap();
        assertThat(extractPutValue(actRes, "raw")).isEqualTo("txt");
    }

    private String extractPutValue(final Put actRes, final String column) {
        final Cell raw = actRes.get(App.SIGNALS_FAMILY, Bytes.toBytes(column))
                        .get(0);
        final byte[] valueArray = raw.getValueArray();
        final int valueOffset = raw.getValueOffset();
        final int valueLength = raw.getValueLength();
        return Bytes.toString(valueArray, valueOffset, valueLength);
    }

    @Test
    public void jsonToPutJsonPresent() throws IOException {
        // Prepare
        final JsonNode jsonNode = new ObjectMapper().readTree(SAMPLE_JSON);
        final IConfigValidator configValidator = mock(IConfigValidator.class);
        final Logger logger = mock(Logger.class);
        final App sut = spy(new App(configValidator, logger));
        final Pair<String, JsonNode> input = Pair
                        .newPair(SAMPLE_JSON, jsonNode);
        final String rowId = "rowId";
        doReturn(rowId).when(sut).generateRowId();

        // Run method under test
        final Put actRes = sut.jsonToPut(input);

        // Verify
        assertThat(actRes.getRow()).isEqualTo(Bytes.toBytes(rowId));
        final NavigableMap<byte[], List<Cell>> x = actRes.getFamilyCellMap();
        assertThat(extractPutValue(actRes, "raw")).isEqualTo(SAMPLE_JSON);
        assertThat(extractPutValue(actRes, "deviceId"))
                        .isEqualTo("11c1310e-c0c2-461b-a4eb-f6bf8da2d23c");
        assertThat(extractPutValue(actRes, "temperature")).isEqualTo("12");
        assertThat(extractPutValue(actRes, "lat")).isEqualTo("10.0");
        assertThat(extractPutValue(actRes, "lon")).isEqualTo("20.0");
        assertThat(extractPutValue(actRes, "time"))
                        .isEqualTo("1970-01-01T03:00:09+03:00");
    }

    @Test
    public void extractDateStringInvalidInput() {
        // Prepare
        final IConfigValidator configValidator = mock(IConfigValidator.class);
        final Logger logger = mock(Logger.class);
        final App sut = spy(new App(configValidator, logger));

        // Run method under test
        final String actRes = sut.extractDateString(App.INVALID_LONG);

        // Verify
        assertThat(actRes).isEqualTo("");
    }

    @Test
    @Parameters({ "0, 1970-01-01T03:00:00+03:00",
                    "1538092800000, 2018-09-28T03:00:00+03:00" })

    public void extractDateStringValidInput(final long millis,
                    final String expRes) {
        // Prepare
        final IConfigValidator configValidator = mock(IConfigValidator.class);
        final Logger logger = mock(Logger.class);
        final App sut = new App(configValidator, logger);

        // Run method under test
        final String actRes = sut.extractDateString(millis);

        // Verify
        assertThat(actRes).isEqualTo(expRes);
    }

    @Test
    public void nullSafeExtractLongNullJson() {
        // Prepare
        final IConfigValidator configValidator = mock(IConfigValidator.class);
        final Logger logger = mock(Logger.class);
        final App sut = new App(configValidator, logger);

        // Run method under test
        final long actRes = sut.nullSafeExtractLong(null, "someProperty");

        // Verify
        assertThat(actRes).isEqualTo(App.INVALID_LONG);
    }

    @Test
    @Parameters({ "abc, -1", "time, 9459", "temperature, 12" })
    public void nullSafeExtractLongNonNullJson(final String property,
                    final long expRes) throws IOException {
        // Prepare
        final IConfigValidator configValidator = mock(IConfigValidator.class);
        final Logger logger = mock(Logger.class);
        final App sut = new App(configValidator, logger);
        final JsonNode jsonNode = new ObjectMapper().readTree(SAMPLE_JSON);

        // Run method under test
        final long actRes = sut.nullSafeExtractLong(jsonNode, property);

        // Verify
        assertThat(actRes).isEqualTo(expRes);
    }

    @Test
    public void nullSafeExtractStringNullJson() {
        // Prepare
        final IConfigValidator configValidator = mock(IConfigValidator.class);
        final Logger logger = mock(Logger.class);
        final App sut = new App(configValidator, logger);

        // Run method under test
        final String actRes = sut.nullSafeExtractString(null, "prop");

        // Verify
        assertThat(actRes).isNotNull().isEmpty();
    }

    @Test
    @Parameters({ "deviceId, 11c1310e-c0c2-461b-a4eb-f6bf8da2d23c",
                    "nonExistentProperty, " })
    public void nullSafeExtractStringNonNullJson(final String propName,
                    final String expRes) throws IOException {
        // Prepare
        final IConfigValidator configValidator = mock(IConfigValidator.class);
        final Logger logger = mock(Logger.class);
        final App sut = new App(configValidator, logger);
        final JsonNode jsonNode = new ObjectMapper().readTree(SAMPLE_JSON);

        // Run method under test
        final String actRes = sut.nullSafeExtractString(jsonNode, propName);

        // Verify
        assertThat(actRes).isEqualTo(expRes);
    }

    @Test
    public void rddToJson() throws IOException {
        // Prepare
        final IConfigValidator configValidator = mock(IConfigValidator.class);
        final Logger logger = mock(Logger.class);
        final App sut = new App(configValidator, logger);
        final JsonNode jsonNode = new ObjectMapper().readTree(SAMPLE_JSON);
        final ConsumerRecord<String, String> rdd = new ConsumerRecord("", 1, 1,
                        "", SAMPLE_JSON);

        // Run method under test
        final Pair<String, JsonNode> actRes = sut
                        .rddToJson(rdd, new ObjectMapper());

        // Verify
        assertThat(actRes).isNotNull();
        assertThat(actRes.getFirst()).isEqualTo(SAMPLE_JSON);
        assertThat(actRes.getSecond()).isEqualTo(jsonNode);
    }
}
