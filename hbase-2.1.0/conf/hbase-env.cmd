@rem/**
@rem * Licensed to the Apache Software Foundation (ASF) under one
@rem * or more contributor license agreements.  See the NOTICE file
@rem * distributed with this work for additional information
@rem * regarding copyright ownership.  The ASF licenses this file
@rem * to you under the Apache License, Version 2.0 (the
@rem * "License"); you may not use this file except in compliance
@rem * with the License.  You may obtain a copy of the License at
@rem *
@rem *     http://www.apache.org/licenses/LICENSE-2.0
@rem *
@rem * Unless required by applicable law or agreed to in writing, software
@rem * distributed under the License is distributed on an "AS IS" BASIS,
@rem * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
@rem * See the License for the specific language governing permissions and
@rem * limitations under the License.
@rem */

@rem Set environment variables here.

set HBASE_HOME=C:\usr\dp\dev\ds-challenge\hbase-2.1.0

@rem The java implementation to use.  Java 1.8+ required.
set JAVA_HOME=C:\Program Files\Java\jdk1.8.0_181

@rem Extra Java CLASSPATH elements.  Optional.
@rem set HBASE_CLASSPATH=

set HBASE_CLASSPATH=C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/aopalliance-repackaged-2.5.0-b32.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/apacheds-i18n-2.0.0-M15.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/apacheds-kerberos-codec-2.0.0-M15.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/api-asn1-api-1.0.0-M20.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/api-util-1.0.0-M20.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/asm-3.1.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/avro-1.7.7.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/commons-beanutils-core-1.8.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/commons-cli-1.2.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/commons-codec-1.10.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/commons-collections-3.2.2.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/commons-compress-1.4.1.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/commons-configuration-1.6.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/commons-crypto-1.0.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/commons-daemon-1.0.13.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/commons-digester-1.8.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/commons-httpclient-3.1.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/commons-io-2.5.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/commons-lang-2.6.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/commons-lang3-3.6.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/commons-math3-3.6.1.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/commons-net-3.1.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/curator-client-4.0.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/curator-framework-4.0.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/curator-recipes-4.0.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/disruptor-3.3.6.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/gson-2.2.4.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/guava-11.0.2.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/guice-3.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/guice-servlet-3.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hadoop-annotations-2.7.4.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hadoop-auth-2.7.4.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hadoop-client-2.7.4.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hadoop-common-2.7.4.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hadoop-common-2.7.4-tests.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hadoop-hdfs-2.7.4.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hadoop-hdfs-2.7.4-tests.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hadoop-mapreduce-client-app-2.7.4.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hadoop-mapreduce-client-common-2.7.4.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hadoop-mapreduce-client-core-2.7.4.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hadoop-mapreduce-client-hs-2.7.4.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hadoop-mapreduce-client-jobclient-2.7.4.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hadoop-mapreduce-client-shuffle-2.7.4.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hadoop-minicluster-2.7.4.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hadoop-yarn-api-2.7.4.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hadoop-yarn-client-2.7.4.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hadoop-yarn-common-2.7.4.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hadoop-yarn-server-applicationhistoryservice-2.7.4.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hadoop-yarn-server-common-2.7.4.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hadoop-yarn-server-nodemanager-2.7.4.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hadoop-yarn-server-resourcemanager-2.7.4.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hadoop-yarn-server-tests-2.7.4-tests.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hadoop-yarn-server-web-proxy-2.7.4.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hamcrest-core-1.3.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-annotations-2.1.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-annotations-2.1.0-tests.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-client-2.1.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-common-2.1.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-common-2.1.0-tests.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-endpoint-2.1.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-examples-2.1.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-external-blockcache-2.1.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-hadoop2-compat-2.1.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-hadoop2-compat-2.1.0-tests.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-hadoop-compat-2.1.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-hadoop-compat-2.1.0-tests.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-http-2.1.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-it-2.1.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-it-2.1.0-tests.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-mapreduce-2.1.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-mapreduce-2.1.0-tests.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-metrics-2.1.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-metrics-api-2.1.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-procedure-2.1.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-protocol-2.1.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-protocol-shaded-2.1.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-replication-2.1.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-resource-bundle-2.1.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-rest-2.1.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-rsgroup-2.1.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-rsgroup-2.1.0-tests.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-server-2.1.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-server-2.1.0-tests.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-shaded-miscellaneous-2.1.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-shaded-netty-2.1.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-shaded-protobuf-2.1.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-shell-2.1.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-testing-util-2.1.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-thrift-2.1.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-zookeeper-2.1.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hbase-zookeeper-2.1.0-tests.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hk2-api-2.5.0-b32.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hk2-locator-2.5.0-b32.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/hk2-utils-2.5.0-b32.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/httpclient-4.5.3.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/httpcore-4.4.6.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jackson-annotations-2.9.2.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jackson-core-2.9.2.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jackson-core-asl-1.9.13.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jackson-databind-2.9.2.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jackson-jaxrs-1.8.3.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jackson-jaxrs-base-2.9.2.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jackson-jaxrs-json-provider-2.9.2.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jackson-mapper-asl-1.9.13.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jackson-module-jaxb-annotations-2.9.2.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jackson-xc-1.8.3.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jamon-runtime-2.4.1.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/javassist-3.20.0-GA.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/javax.annotation-api-1.2.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/javax.el-3.0.1-b08.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/javax.inject-2.5.0-b32.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/javax.servlet.jsp.jstl-1.2.0.v201105211821.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/javax.servlet.jsp.jstl-1.2.2.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/javax.servlet.jsp-2.3.2.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/javax.servlet.jsp-api-2.3.1.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/javax.servlet-api-3.1.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/javax.ws.rs-api-2.0.1.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/java-xmlbuilder-0.4.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jaxb-api-2.2.12.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jaxb-impl-2.2.3-1.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jcodings-1.0.18.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jersey-client-2.25.1.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jersey-common-2.25.1.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jersey-container-servlet-core-2.25.1.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jersey-guava-2.25.1.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jersey-media-jaxb-2.25.1.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jersey-server-2.25.1.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jets3t-0.9.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jettison-1.3.8.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jetty-6.1.26.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jetty-http-9.3.19.v20170502.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jetty-io-9.3.19.v20170502.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jetty-jmx-9.3.19.v20170502.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jetty-jsp-9.2.19.v20160908.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jetty-schemas-3.1.M0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jetty-security-9.3.19.v20170502.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jetty-server-9.3.19.v20170502.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jetty-servlet-9.3.19.v20170502.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jetty-sslengine-6.1.26.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jetty-util-6.1.26.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jetty-util-9.3.19.v20170502.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jetty-util-ajax-9.3.19.v20170502.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jetty-webapp-9.3.19.v20170502.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jetty-xml-9.3.19.v20170502.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/joni-2.1.11.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/jsch-0.1.54.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/junit-4.12.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/leveldbjni-all-1.8.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/libthrift-0.9.3.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/metrics-core-3.2.1.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/netty-all-4.0.23.Final.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/org.eclipse.jdt.core-3.8.2.v20130121.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/osgi-resource-locator-1.0.1.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/paranamer-2.3.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/protobuf-java-2.5.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/snappy-java-1.0.5.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/spymemcached-2.12.2.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/validation-api-1.1.0.Final.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/xmlenc-0.52.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/xz-1.0.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/zookeeper-3.4.10.jar;C:/usr/dp/dev/ds-challenge/hbase-2.1.0/lib/commons-logging-1.2.jar


@rem The maximum amount of heap to use. Default is left to JVM default.
@rem set HBASE_HEAPSIZE=1000

@rem Uncomment below if you intend to use off heap cache. For example, to allocate 8G of 
@rem offheap, set the value to "8G".
@rem set HBASE_OFFHEAPSIZE=1000

@rem For example, to allocate 8G of offheap, to 8G:
@rem etHBASE_OFFHEAPSIZE=8G

@rem Extra Java runtime options.
@rem Below are what we set by default.  May only work with SUN JVM.
@rem For more on why as well as other possible settings,
@rem see http://hbase.apache.org/book.html#performance
@rem JDK6 on Windows has a known bug for IPv6, use preferIPv4Stack unless JDK7.
@rem @rem See TestIPv6NIOServerSocketChannel.
set HBASE_OPTS="-XX:+UseConcMarkSweepGC" "-Djava.net.preferIPv4Stack=true"

@rem Uncomment below to enable java garbage collection logging for the server-side processes
@rem this enables basic gc logging for the server processes to the .out file
@rem set SERVER_GC_OPTS="-verbose:gc" "-XX:+PrintGCDetails" "-XX:+PrintGCDateStamps" %HBASE_GC_OPTS%

@rem this enables gc logging using automatic GC log rolling. Only applies to jdk 1.6.0_34+ and 1.7.0_2+. Either use this set of options or the one above
@rem set SERVER_GC_OPTS="-verbose:gc" "-XX:+PrintGCDetails" "-XX:+PrintGCDateStamps" "-XX:+UseGCLogFileRotation" "-XX:NumberOfGCLogFiles=1" "-XX:GCLogFileSize=512M" %HBASE_GC_OPTS%

@rem Uncomment below to enable java garbage collection logging for the client processes in the .out file.
@rem set CLIENT_GC_OPTS="-verbose:gc" "-XX:+PrintGCDetails" "-XX:+PrintGCDateStamps" %HBASE_GC_OPTS%

@rem Uncomment below (along with above GC logging) to put GC information in its own logfile (will set HBASE_GC_OPTS)
@rem set HBASE_USE_GC_LOGFILE=true

@rem Uncomment and adjust to enable JMX exporting
@rem See jmxremote.password and jmxremote.access in $JRE_HOME/lib/management to configure remote password access.
@rem More details at: http://java.sun.com/javase/6/docs/technotes/guides/management/agent.html
@rem
@rem set HBASE_JMX_BASE="-Dcom.sun.management.jmxremote.ssl=false" "-Dcom.sun.management.jmxremote.authenticate=false"
@rem set HBASE_MASTER_OPTS=%HBASE_JMX_BASE% "-Dcom.sun.management.jmxremote.port=10101"
@rem set HBASE_REGIONSERVER_OPTS=%HBASE_JMX_BASE% "-Dcom.sun.management.jmxremote.port=10102"
@rem set HBASE_THRIFT_OPTS=%HBASE_JMX_BASE% "-Dcom.sun.management.jmxremote.port=10103"
@rem set HBASE_ZOOKEEPER_OPTS=%HBASE_JMX_BASE% -Dcom.sun.management.jmxremote.port=10104"

@rem File naming hosts on which HRegionServers will run.  $HBASE_HOME/conf/regionservers by default.
@rem set HBASE_REGIONSERVERS=%HBASE_HOME%\conf\regionservers

@rem Where log files are stored.  $HBASE_HOME/logs by default.
@rem set HBASE_LOG_DIR=%HBASE_HOME%\logs

@rem A string representing this instance of hbase. $USER by default.
@rem set HBASE_IDENT_STRING=%USERNAME%

@rem Seconds to sleep between slave commands.  Unset by default.  This
@rem can be useful in large clusters, where, e.g., slave rsyncs can
@rem otherwise arrive faster than the master can service them.
@rem set HBASE_SLAVE_SLEEP=0.1

@rem Tell HBase whether it should manage it's own instance of ZooKeeper or not.
@rem set HBASE_MANAGES_ZK=true
