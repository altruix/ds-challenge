package cc.altruix.dschallenge.commons;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Properties;
import java.util.function.Predicate;

public class DateTimePredicate implements Predicate<String> {
    private final Properties cfg;
    private final String formatString;

    public DateTimePredicate(
                    final Properties cfg, 
                    final String formatString) {
        this.cfg = cfg;
        this.formatString = formatString;
    }

    @Override
    public boolean test(final String propName) {
        final String propVal = cfg.getProperty(propName);
        final SimpleDateFormat format = new SimpleDateFormat(formatString);
        try {
            format.parse(propVal);
            return true;
        } catch (final ParseException exception) {
            return false;
        }
    }
}
