package cc.altruix.dschallenge.commons;

public class ValidationResult {
    private final boolean valid;
    private final String message;

    public ValidationResult(final boolean valid,
                    final String message) {
        this.valid = valid;
        this.message = message;
    }

    public boolean isValid() {
        return valid;
    }

    public String getMessage() {
        return message;
    }
}
