package cc.altruix.dschallenge.commons;

import java.util.Properties;
import java.util.function.Predicate;

import org.apache.commons.lang3.StringUtils;

public class NonNumericPropValuePredicate implements Predicate<String> {
    public static final String ALLOWED_CHARS = "0123456789-";
    private final Properties config;

    public NonNumericPropValuePredicate(final Properties config) {
        this.config = config;
    }

    @Override
    public boolean test(final String propName) {
        final String propVal = config.getProperty(propName);
        if (propVal.length() == 1) {
            return !StringUtils.isNumeric(propVal);
        }
        return !StringUtils.containsOnly(propVal, ALLOWED_CHARS);
    }
}
