package cc.altruix.dschallenge.commons;

import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.function.Predicate;

import org.apache.commons.lang3.tuple.Pair;

public abstract class AbstractValidator implements IConfigValidator {
    static final String NULL_CONFIG = "null config";
    static final String EMPTY_CONFIG = "Empty config";
    static final String EMPTY_PROPERTY_MSG = "Property '%s' is empty";
    static final String NON_NUMERIC_PROPERTY_MSG = "Property '%s' is not numeric";
    static final String DATETIME_ERROR_MSG = "Property '%s' is not a datetime";
    static final String DATETIME_FORMAT_STRING = "yyyy-MM-dd HH:mm";

    @Override
    public ValidationResult validate(final Properties cfg) {
        if (cfg == null) {
            return new ValidationResult(false, NULL_CONFIG);
        }
        if (cfg.isEmpty()) {
            return new ValidationResult(false, EMPTY_CONFIG);
        }

        final ValidationResult emptyPropValRes = validateProps(
                        EMPTY_PROPERTY_MSG, createEmptyPropValuePredicate(cfg),
                        getNonEmptyProps());
        if (!emptyPropValRes.isValid()) {
            return emptyPropValRes;
        }
        final ValidationResult nonNumericPropValRes = validateProps(
                        NON_NUMERIC_PROPERTY_MSG,
                        createNonNumericPropValuePredicate(cfg),
                        getNumericProps());
        if (!nonNumericPropValRes.isValid()) {
            return nonNumericPropValRes;
        }
        final ValidationResult datePropValRes = validateProps(
                        DATETIME_ERROR_MSG, createDateTimePredicate(cfg),
                        getDateTimeProps());
        if (!datePropValRes.isValid()) {
            return datePropValRes;
        }
        final ValidationResult minMaxValRes = validateMinMaxPairs(cfg);
        if (!minMaxValRes.isValid()) {
            return minMaxValRes;
        }
        return new ValidationResult(true, "");
    }

    protected Predicate<String> createDateTimePredicate(Properties cfg) {
        return new DateTimePredicate(cfg, DATETIME_FORMAT_STRING);
    }

    protected Predicate<String> createNonNumericPropValuePredicate(
                    Properties cfg) {
        return new NonNumericPropValuePredicate(cfg);
    }

    protected Predicate<String> createEmptyPropValuePredicate(Properties cfg) {
        return new EmptyPropValuePredicate(cfg);
    }

    protected ValidationResult validateMinMaxPairs(final Properties cfg) {
        final MinMaxValuePredicate pred = new MinMaxValuePredicate(cfg);
        final Optional<Pair<String, String>> wrongMinMaxVal = this
                        .getMinMaxPairs().stream().filter(pred).findFirst();
        if (wrongMinMaxVal.isPresent()) {
            final Pair<String, String> minMax = wrongMinMaxVal.get();
            final String minProp = minMax.getLeft();
            final String maxProp = minMax.getRight();
            final String msg = String.format("Minimum is not less than"
                                            + " maximum in the min-max pair (%s,%s)", minProp,
                            maxProp);
            return new ValidationResult(false, msg);
        }
        return new ValidationResult(true, "");
    }

    protected ValidationResult validateProps(
                    final String msgFormatString,
                    final Predicate<String> errorPredicate,
                    final List<String> propsToCheck) {
        final Optional<String> wrongPropOpt = propsToCheck.stream()
                        .filter(errorPredicate).findFirst();
        if (wrongPropOpt.isPresent()) {
            final String propName = wrongPropOpt.get();
            return new ValidationResult(false,
                            String.format(msgFormatString, propName));
        }
        return new ValidationResult(true, "");
    }

    protected abstract List<String> getNonEmptyProps();

    protected abstract List<String> getNumericProps();

    protected abstract List<String> getDateTimeProps();

    protected abstract List<Pair<String, String>> getMinMaxPairs();
}
