package cc.altruix.dschallenge.commons;

import java.util.Properties;

public interface IConfigValidator {
    ValidationResult validate(Properties cfg);
}
