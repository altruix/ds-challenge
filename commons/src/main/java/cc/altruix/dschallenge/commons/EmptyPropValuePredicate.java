package cc.altruix.dschallenge.commons;

import java.util.Properties;
import java.util.function.Predicate;

import org.apache.commons.lang3.StringUtils;

public class EmptyPropValuePredicate implements Predicate<String> {
    private final Properties config;

    public EmptyPropValuePredicate(final Properties config) {
        this.config = config;
    }

    @Override
    public boolean test(final String propName) {
        final String propVal = config.getProperty(propName);
        return StringUtils.isBlank(propVal);
    }
}
