package cc.altruix.dschallenge.commons;

import java.util.Properties;
import java.util.function.Predicate;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.tuple.Pair;

public class MinMaxValuePredicate 
                implements Predicate<Pair<String, String>> {
    private final Properties cfg;

    public MinMaxValuePredicate(final Properties cfg) {
        this.cfg = cfg;
    }

    @Override
    public boolean test(final Pair<String, String> propValue) {
        final String minValTxt = cfg.getProperty(propValue.getLeft());
        final String maxValTxt = cfg.getProperty(propValue.getRight());
        final double min = NumberUtils.toDouble(minValTxt);
        final double max = NumberUtils.toDouble(maxValTxt);
        return !(min <= max);
    }
}
