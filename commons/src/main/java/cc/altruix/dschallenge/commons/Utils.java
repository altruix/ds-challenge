package cc.altruix.dschallenge.commons;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;

public final class Utils {
    private Utils() {

    }

    public static Properties readConfig(final String configFileName,
                    final Logger logger) {
        final Properties config = new Properties();
        try (final FileInputStream fis = new FileInputStream(
                        new File(configFileName))) {
            config.load(fis);
        } catch (final IOException e) {
            logger.error("Error reading config", e);
        }
        return config;
    }

}
