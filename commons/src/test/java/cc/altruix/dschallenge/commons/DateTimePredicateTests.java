package cc.altruix.dschallenge.commons;

import java.util.Properties;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.fest.assertions.api.Assertions.assertThat;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class DateTimePredicateTests {
    @Test
    @Parameters({", false",
                    "2018-09-11 23:59, true",
                    "2018-09-11 00:00, true",
                    "2018-09-11T00:00.999, false"
    })
    public void testTestLogic(final String propValue,
                    final boolean expRes) {
        // Prepare
        final String propName = "property";
        final Properties cfg = new Properties();
        cfg.setProperty(propName, propValue);
        final DateTimePredicate sut = new DateTimePredicate(cfg, 
                        AbstractValidator.DATETIME_FORMAT_STRING);

        // Run method under test
        final boolean actRes = sut.test(propName);
        
        // Verify
        assertThat(actRes).isEqualTo(expRes);
    }
}
