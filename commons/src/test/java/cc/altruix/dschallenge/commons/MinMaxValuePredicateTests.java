package cc.altruix.dschallenge.commons;

import java.util.Properties;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.fest.assertions.api.Assertions.assertThat;

import org.apache.commons.lang3.tuple.Pair;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class MinMaxValuePredicateTests {
    @Test
    @Parameters({"-10, 10, false",
                    "10, -10, true"
    })
    public void test(final String minVal, 
                    final String maxVal, 
                    final boolean expRes) {
        // Prepare
        final Properties cfg = new Properties();
        cfg.setProperty("min", minVal);
        cfg.setProperty("max", maxVal);
        final MinMaxValuePredicate sut = new MinMaxValuePredicate(cfg);
        
        // Run method under test
        final boolean actRes = sut.test(Pair.of("min", "max"));
        
        // Verify
        assertThat(actRes).isEqualTo(expRes);
    }
}
