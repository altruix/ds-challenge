package cc.altruix.dschallenge.commons;

import java.util.Properties;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.fest.assertions.api.Assertions.assertThat;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class NonNumericPropValuePredicateTests {

    public static final String PROP_NAME = "prop";

    @Test
    @Parameters({"-10, false",
                    "10, false",
                    "NaN, true",
                    "a, true",
                    "-, true"
    })
    public void test(final String propVal, final boolean expRes) {
        // Prepare
        final Properties cfg = new Properties();
        cfg.setProperty(PROP_NAME, propVal);
        final NonNumericPropValuePredicate sut = 
                        new NonNumericPropValuePredicate(cfg);

        // Run method under test
        final boolean actRes = sut.test(PROP_NAME);
        
        // Verify
        assertThat(actRes).isEqualTo(expRes);
    }
}
