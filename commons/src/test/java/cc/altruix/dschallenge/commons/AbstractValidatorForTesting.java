package cc.altruix.dschallenge.commons;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

public class AbstractValidatorForTesting extends AbstractValidator {
    
    @Override
    protected List<String> getNonEmptyProps() {
        return new ArrayList<>(0);
    }

    @Override
    protected List<String> getNumericProps() {
        return new ArrayList<>(0);
    }

    @Override
    protected List<String> getDateTimeProps() {
        return new ArrayList<>(0);
    }

    @Override
    protected List<Pair<String, String>> getMinMaxPairs() {
        return new ArrayList<>(0);
    }
}
