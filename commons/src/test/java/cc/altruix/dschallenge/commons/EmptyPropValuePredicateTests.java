package cc.altruix.dschallenge.commons;

import java.util.Properties;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.fest.assertions.api.Assertions.assertThat;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class EmptyPropValuePredicateTests {

    public static final String PROP_NAME = "propName";

    @Test
    @Parameters({", true",
                    "  , true",
                    "Some text, false",
                    "123, false"
    })
    public void test(final String propVal, final boolean expRes) {
        // Prepare
        final Properties cfg = new Properties();
        cfg.setProperty(PROP_NAME, propVal);
        final EmptyPropValuePredicate sut = new EmptyPropValuePredicate(cfg);
        
        // Run method under test
        final boolean actRes = sut.test(PROP_NAME);
        
        // Verify
        assertThat(actRes).isEqualTo(expRes);
    }
    @Test
    public void testOnNull() {
        // Prepare
        final EmptyPropValuePredicate sut = new EmptyPropValuePredicate(new Properties());

        // Run method under test
        final boolean actRes = sut.test(PROP_NAME);

        // Verify
        assertThat(actRes).isTrue();
    }
}
