package cc.altruix.dschallenge.commons;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.function.Predicate;

import org.junit.Test;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.apache.commons.lang3.tuple.Pair;

public class AbstractValidatorTests {
    @Test
    public void validateOnNullConfig() {
        // Prepare
        final AbstractValidator sut = new AbstractValidatorForTesting();

        // Run method under test
        final ValidationResult actRes = sut.validate(null);

        // Verify
        assertThat(actRes).isNotNull();
        assertThat(actRes.isValid()).isFalse();
        assertThat(actRes.getMessage())
                        .isEqualTo(AbstractValidator.NULL_CONFIG);
    }

    @Test
    public void validateOnEmptyConfig() {
        // Prepare
        final AbstractValidator sut = new AbstractValidatorForTesting();

        // Run method under test
        final ValidationResult actRes = sut.validate(new Properties());

        // Verify
        assertThat(actRes).isNotNull();
        assertThat(actRes.isValid()).isFalse();
        assertThat(actRes.getMessage())
                        .isEqualTo(AbstractValidator.EMPTY_CONFIG);
    }

    @Test
    public void validateOnEmptyProperty() {
        // Prepare
        final AbstractValidator sut = spy(new AbstractValidatorForTesting());
        final Properties cfg = mock(Properties.class);
        final Predicate<String> pred = mock(Predicate.class);
        final List<String> propNames = mock(List.class);
        final ValidationResult expRes = new ValidationResult(false, "abc");

        doReturn(pred).when(sut).createEmptyPropValuePredicate(cfg);
        doReturn(propNames).when(sut).getNonEmptyProps();
        doReturn(expRes).when(sut)
                        .validateProps(AbstractValidator.EMPTY_PROPERTY_MSG,
                                        pred, propNames);

        // Run method under test
        final ValidationResult actRes = sut.validate(cfg);

        // Verify
        assertThat(actRes).isSameAs(expRes);
        verify(sut).validateProps(AbstractValidator.EMPTY_PROPERTY_MSG, pred,
                        propNames);
    }

    @Test
    public void validateOnNonNumericProperty() {
        // Prepare
        final AbstractValidator sut = spy(new AbstractValidatorForTesting());
        final Properties cfg = mock(Properties.class);
        final Predicate<String> pred = mock(Predicate.class);
        final List<String> propNames = mock(List.class);
        final ValidationResult expRes = new ValidationResult(false, "abc");

        doReturn(pred).when(sut).createNonNumericPropValuePredicate(cfg);
        doReturn(propNames).when(sut).getNumericProps();
        doReturn(expRes).when(sut)
                        .validateProps(AbstractValidator.NON_NUMERIC_PROPERTY_MSG,
                                        pred, propNames);

        // Run method under test
        final ValidationResult actRes = sut.validate(cfg);

        // Verify
        assertThat(actRes).isSameAs(expRes);
        verify(sut).validateProps(AbstractValidator.NON_NUMERIC_PROPERTY_MSG,
                        pred, propNames);
    }

    @Test
    public void validateDateTimeProperty() {
        // Prepare
        final AbstractValidator sut = spy(new AbstractValidatorForTesting());
        final Properties cfg = mock(Properties.class);
        final Predicate<String> pred = mock(Predicate.class);
        final List<String> propNames = mock(List.class);
        final ValidationResult expRes = new ValidationResult(false, "abc");

        doReturn(pred).when(sut).createDateTimePredicate(cfg);
        doReturn(propNames).when(sut).getDateTimeProps();
        doReturn(expRes).when(sut)
                        .validateProps(AbstractValidator.DATETIME_ERROR_MSG,
                                        pred, propNames);

        // Run method under test
        final ValidationResult actRes = sut.validate(cfg);

        // Verify
        assertThat(actRes).isSameAs(expRes);
        verify(sut).validateProps(AbstractValidator.DATETIME_ERROR_MSG, pred,
                        propNames);
    }

    @Test
    public void validateMinMaxViolation() {
        // Prepare
        final AbstractValidator sut = spy(new AbstractValidatorForTesting());
        final Properties cfg = mock(Properties.class);
        final ValidationResult expRes = new ValidationResult(false, "abc");
        final List<Pair<String, String>> minMaxPairs = mock(List.class);
        doReturn(minMaxPairs).when(sut).getMinMaxPairs();
        doReturn(expRes).when(sut).validateMinMaxPairs(cfg);

        // Run method under test
        final ValidationResult actRes = sut.validate(cfg);

        // Verify
        assertThat(actRes).isSameAs(expRes);
        verify(sut).validateMinMaxPairs(cfg);
    }

    @Test
    public void validateNoErrors() {
        // Prepare
        final AbstractValidator sut = spy(new AbstractValidatorForTesting());
        final Properties cfg = mock(Properties.class);

        // Run method under test
        final ValidationResult actRes = sut.validate(cfg);

        // Verify
        assertThat(actRes).isNotNull();
        assertThat(actRes.isValid()).isTrue();
        assertThat(actRes.getMessage()).isEmpty();
    }

    @Test
    public void validateMinMaxPairsError() {
        validateMinMaxPairsTestLogic(Pair.of("max", "min"), false,
                        "Minimum is not less than"
                                        + " maximum in the min-max pair (max,min)");
    }

    @Test
    public void validateMinMaxPairsNoError() {
        validateMinMaxPairsTestLogic(Pair.of("min", "max"), true,
                        "");
    }
    
    @Test
    public void validatePropsError() {
        validatePropsTestLogic(true, false, 
                        "Wrong property");
    }
    @Test
    public void validatePropsNoError() {
        validatePropsTestLogic(false, true,
                        "");
    }
    protected void validatePropsTestLogic(
                    final boolean predRetVal,
                    final boolean expValid,
                    final String expError) {
        // Prepare
        final AbstractValidator sut = spy(new AbstractValidatorForTesting());
        final Properties cfg = mock(Properties.class);
        final String propName = "property";
        final Predicate<String> pred = mock(Predicate.class);
        when(pred.test(propName)).thenReturn(predRetVal);
        
        final String errorFormatString = "Wrong %s";
        
        // Run method under test
        final ValidationResult actRes = sut
                        .validateProps(errorFormatString, pred,
                                        Arrays.asList(propName));

        // Verify
        assertThat(actRes).isNotNull();
        assertThat(actRes.isValid()).isEqualTo(expValid);
        assertThat(actRes.getMessage()).isEqualTo(expError);
        verify(pred).test(propName);
    }

    public void validateMinMaxPairsTestLogic(final Pair<String, String> pair,
                    final boolean expValid, final String expMsg) {
        // Prepare
        final AbstractValidator sut = spy(new AbstractValidatorForTesting());
        final Properties cfg = mock(Properties.class);
        final String minPropName = "min";
        final String maxPropName = "max";
        final List<Pair<String, String>> minMaxPairs = Arrays.asList(pair);
        doReturn(minMaxPairs).when(sut).getMinMaxPairs();
        when(cfg.getProperty(minPropName)).thenReturn("-10");
        when(cfg.getProperty(maxPropName)).thenReturn("10");

        // Run method under test
        final ValidationResult actRes = sut.validateMinMaxPairs(cfg);

        // Verify
        assertThat(actRes.isValid()).isEqualTo(expValid);
        assertThat(actRes.getMessage()).isEqualTo(expMsg);
    }
}
