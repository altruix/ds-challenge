package cc.altruix.dschallenge.task1;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import static org.fest.assertions.api.Assertions.assertThat;

import org.apache.commons.lang3.tuple.Pair;

public class ConfigValidatorTests {
    private final static List<String> DATE_TIME_PROPS = Arrays
                    .asList("dev1.time.min", "dev1.time.max", "dev2.time.min",
                                    "dev2.time.max", "dev3.time.min",
                                    "dev3.time.max");
    private final static List<String> NUMERIC_PROPS = Arrays
                    .asList("dev1.temperature.min", "dev1.temperature.max",
                                    "dev1.lat.min", "dev1.lat.max",
                                    "dev1.lon.min", "dev1.lon.max",
                                    "dev2.temperature.min",
                                    "dev2.temperature.max", "dev2.lat.min",
                                    "dev2.lat.max", "dev2.lon.min",
                                    "dev2.lon.max", "dev3.temperature.min",
                                    "dev3.temperature.max", "dev3.lat.min",
                                    "dev3.lat.max", "dev3.lon.min",
                                    "dev3.lon.max", "retries", "batch.size",
                                    "linger.ms", "buffer.memory");
    private final static List<String> NON_EMPTY_PROPS = Arrays
                    .asList("dev1.id", "dev2.id", "dev3.id",
                                    "dev1.temperature.min",
                                    "dev1.temperature.max", "dev1.lat.min",
                                    "dev1.lat.max", "dev1.lon.min",
                                    "dev1.lon.max", "dev1.time.min",
                                    "dev1.time.max", "dev2.temperature.min",
                                    "dev2.temperature.max", "dev2.lat.min",
                                    "dev2.lat.max", "dev2.lon.min",
                                    "dev2.lon.max", "dev2.time.min",
                                    "dev2.time.max", "dev3.temperature.min",
                                    "dev3.temperature.max", "dev3.lat.min",
                                    "dev3.lat.max", "dev3.lon.min",
                                    "dev3.lon.max", "dev3.time.min",
                                    "dev3.time.max", "bootstrap.servers", "acks",
                                    "retries", "batch.size", "linger.ms",
                                    "buffer.memory", "key.serializer",
                                    "value.serializer", "targetTopic");

    @Test
    public void createDateTimeProps() {
        // Prepare
        final ConfigValidator sut = new ConfigValidator();

        // Run method under test
        final List<String> actRes = sut.createDateTimeProps();

        // Verify
        assertActResEqual(actRes, DATE_TIME_PROPS);
    }

    @Test
    public void createNumericProps() {
        // Prepare
        final ConfigValidator sut = new ConfigValidator();

        // Run method under test
        final List<String> actRes = sut.createNumericProps();

        // Verify
        assertActResEqual(actRes, NUMERIC_PROPS);
    }

    @Test
    public void createNonEmptyProps() {
        // Prepare
        final ConfigValidator sut = new ConfigValidator();

        // Run method under test
        final List<String> actRes = sut.createNonEmptyProps();

        // Verify
        assertActResEqual(actRes, NON_EMPTY_PROPS);
    }
    @Test
    public void createMinMaxPairs() {
        // Prepare
        final ConfigValidator sut = new ConfigValidator();
        
        // Run method under test
        final List<Pair<String, String>> actRes = sut.createMinMaxPairs();
        
        // Verify
        assertThat(actRes).isNotNull().contains(
            Pair.of("dev1.lat.min", "dev1.lat.max"),
            Pair.of("dev1.lon.min", "dev1.lon.max"),
            Pair.of("dev1.temperature.min", "dev1.temperature.max"),
            Pair.of("dev2.lat.min", "dev2.lat.max"),
            Pair.of("dev2.lon.min", "dev2.lon.max"),
            Pair.of("dev2.temperature.min", "dev2.temperature.max"),
            Pair.of("dev3.lat.min", "dev3.lat.max"),
            Pair.of("dev3.lon.min", "dev3.lon.max"),
            Pair.of("dev3.temperature.min", "dev3.temperature.max")
        ).hasSize(9);
    }

    private void assertActResEqual(final List<String> actRes,
                    final List<String> expRes) {
        assertThat(actRes.size()).isEqualTo(expRes.size());
        expRes.stream().forEach(prop -> assertThat(actRes).contains(prop));
    }
}
