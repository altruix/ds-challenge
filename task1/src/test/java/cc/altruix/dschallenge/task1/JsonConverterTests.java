package cc.altruix.dschallenge.task1;

import org.junit.Test;
import static org.fest.assertions.api.Assertions.assertThat;

public class JsonConverterTests {
    @Test
    public void toJson() {
        // Prepare
        final JsonConverter sut = new JsonConverter();
        final Location loc = new Location(10.0, 20.0);
        final SimulatedDataItem data = new SimulatedDataItem(
                        "11c1310e-c0c2-461b-a4eb-f6bf8da2d23c",
                        12,
                        loc,
                        9459L
        );
        
        // Run method under test
        final String actRes = sut.toJson(data);
        
        // Verify
        assertThat(actRes).isEqualTo("{\"deviceId\":\"11c1310e-c0c2-461b-a4eb-f6bf8da2d23c\",\"temperature\":12,\"location\":{\"latitude\":\"10.0\",\"longitude\":\"20.0\"},\"time\":\"9459\"}");
    }
}
