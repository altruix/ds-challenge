package cc.altruix.dschallenge.task1;

import java.util.Properties;

import org.junit.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.mockito.verification.VerificationMode;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.slf4j.Logger;

import cc.altruix.dschallenge.commons.IConfigValidator;
import cc.altruix.dschallenge.commons.ValidationResult;

public class AppTests {

    private static final String MSG = "msg";

    @Test
    public void runConfigError() {
        runTestLogic(new ValidationResult(false, MSG), 
                        never(), times(1));
    }
    @Test
    public void runConfigNoError() {
        runTestLogic(new ValidationResult(true, MSG),
                        times(1), never());
    }
    @Test
    public void startSchedulerNoError() throws SchedulerException {
        // Prepare
        final IConfigValidator configValidator = mock(IConfigValidator.class);
        final Logger logger = mock(Logger.class);
        final App sut = spy(new App(configValidator, logger));
        final Properties cfg = mock(Properties.class);
        final Scheduler scheduler = mock(Scheduler.class);
        doReturn(scheduler).when(sut).createScheduler();
        final JobDetail job = mock(JobDetail.class);
        doReturn(job).when(sut).createJobDetail(cfg);
        final Trigger trigger = mock(Trigger.class);
        doReturn(trigger).when(sut).createTrigger();
        
        // Run method under test
        sut.startScheduler(cfg);
        
        // Verify
        verify(sut).createScheduler();
        verify(sut).createJobDetail(cfg);
        verify(sut).createTrigger();
        verify(scheduler).scheduleJob(job, trigger);
        verify(scheduler).start();
        verify(logger, never()).error(anyString(), any(SchedulerException.class));
    }
    @Test
    public void startSchedulerError() throws SchedulerException {
        // Prepare
        final IConfigValidator configValidator = mock(IConfigValidator.class);
        final Logger logger = mock(Logger.class);
        final App sut = spy(new App(configValidator, logger));
        final Properties cfg = mock(Properties.class);
        final Scheduler scheduler = mock(Scheduler.class);
        doReturn(scheduler).when(sut).createScheduler();
        final JobDetail job = mock(JobDetail.class);
        doReturn(job).when(sut).createJobDetail(cfg);
        final Trigger trigger = mock(Trigger.class);
        doReturn(trigger).when(sut).createTrigger();
        final SchedulerException exception = new SchedulerException(
                        MSG);
        doThrow(exception).when(scheduler).start();
        
        // Run method under test
        sut.startScheduler(cfg);

        // Verify
        verify(sut).createScheduler();
        verify(sut).createJobDetail(cfg);
        verify(sut).createTrigger();
        verify(scheduler).scheduleJob(job, trigger);
        verify(scheduler).start();
        verify(logger, times(1)).error(
                        "Could not create scheduler", 
                        exception
        );
    }
    private void runTestLogic(final ValidationResult cfgValRes,
                    final VerificationMode startSchedulerCallCount,
                    final VerificationMode loggerErrorInvocationCount) {
        // Prepare
        final IConfigValidator configValidator = mock(IConfigValidator.class);
        final Logger logger = mock(Logger.class);
        final App sut = spy(new App(configValidator, logger));
        final Properties cfg = mock(Properties.class);
        doReturn(cfg).when(sut).readConfig();
        when(configValidator.validate(cfg)).thenReturn(cfgValRes);
        doNothing().when(sut).startScheduler(cfg);
        
        // Run method under test
        sut.run();

        // Verify
        verify(sut).readConfig();
        verify(configValidator).validate(cfg);
        verify(logger, loggerErrorInvocationCount).error(String
                        .format("There is a problem with the configuration file: %s",
                                        MSG));
        verify(sut, startSchedulerCallCount).startScheduler(cfg);
    }

}
