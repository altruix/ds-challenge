package cc.altruix.dschallenge.task1;

import java.util.Properties;

import org.junit.Test;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.fest.assertions.data.MapEntry;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;

public class SimulatorJobTests {

    public static final String TARGET_TOPIC_X = "targetTopicX";

    @Test
    public void execute() {
        // Prepare
        final ISimulatedDataGenerator simulatedDataGenerator = 
                        mock(ISimulatedDataGenerator.class);
        final IJsonConverter jsonConverter = mock(IJsonConverter.class);
        final SimulatorJob sut = spy(new SimulatorJob(simulatedDataGenerator, 
                        jsonConverter));
        final JobExecutionContext ctx = mock(JobExecutionContext.class);
        final JobDetail jobDetail = mock(JobDetail.class);
        when(ctx.getJobDetail()).thenReturn(jobDetail);
        final JobDataMap jdm = mock(JobDataMap.class);
        when(jobDetail.getJobDataMap()).thenReturn(jdm);
        final Properties kafkaProps = mock(Properties.class);
        doReturn(kafkaProps).when(sut).transformProps(jdm);
        when(jdm.getString(SimulatorJob.TARGET_TOPIC)).thenReturn(
                        TARGET_TOPIC_X);
        final Properties config = mock(Properties.class);
        doReturn(config).when(sut).extractConfig(jdm);
        final SimulatedDataItem simulatedDataItem = mock(SimulatedDataItem.class);
        when(simulatedDataGenerator.generateData(config))
                        .thenReturn(simulatedDataItem);
        final String json = "json";
        when(jsonConverter.toJson(simulatedDataItem)).thenReturn(json);
        doNothing().when(sut).sendMessageToKafka(kafkaProps, TARGET_TOPIC_X, 
                        json);
        
        // Run method under test
        sut.execute(ctx);
        
        // Verify
        verify(ctx).getJobDetail();
        verify(jobDetail).getJobDataMap();
        verify(sut).transformProps(jdm);
        verify(jdm).getString(SimulatorJob.TARGET_TOPIC);
        verify(sut).extractConfig(jdm);
        verify(simulatedDataGenerator).generateData(config);
        verify(jsonConverter).toJson(simulatedDataItem);
        verify(sut).sendMessageToKafka(kafkaProps, TARGET_TOPIC_X, 
                        json);
    }
    @Test
    public void extractConfigExtractsOnlyStringValues() {
        // Prepare
        final ISimulatedDataGenerator simulatedDataGenerator =
                        mock(ISimulatedDataGenerator.class);
        final IJsonConverter jsonConverter = mock(IJsonConverter.class);
        final SimulatorJob sut = new SimulatorJob(simulatedDataGenerator,
                        jsonConverter);
        final JobDataMap jdm = new JobDataMap();
        jdm.put("string", "abc");
        jdm.put("int", 1);
        jdm.put("long", 2L);
        jdm.put("boolean", false);
        jdm.put("object", Object.class);
        
        // Run method under test
        final Properties actRes = sut.extractConfig(jdm);

        // Verify
        assertThat(actRes).containsKey("string")
                        .contains(MapEntry.entry("string", "abc"))
        .doesNotContainKey("int")
        .doesNotContainKey("long")
        .doesNotContainKey("boolean")
        .doesNotContainKey("object");
    }
    @Test
    public void transformProps() {
        // Prepare
        final JobDataMap jdm = new JobDataMap();
        jdm.put("bootstrap.servers", "bootstrap.servers.value");
        jdm.put("acks", "acks.value");
        jdm.put("retries", 10);
        jdm.put("batch.size", 20);
        jdm.put("linger.ms", 1000);
        jdm.put("buffer.memory", 100);
        jdm.put("key.serializer", "key.serializer.value");
        jdm.put("value.serializer", "value.serializer.value");

        final ISimulatedDataGenerator simulatedDataGenerator =
                        mock(ISimulatedDataGenerator.class);
        final IJsonConverter jsonConverter = mock(IJsonConverter.class);
        final SimulatorJob sut = new SimulatorJob(simulatedDataGenerator,
                        jsonConverter);
        
        // Run method under test
        final Properties actRes = sut.transformProps(jdm);

        // Verify
        assertThat(actRes).contains(
                        MapEntry.entry("bootstrap.servers", "bootstrap.servers.value"),
                        MapEntry.entry("acks", "acks.value"),
                        MapEntry.entry("retries", 10),
                        MapEntry.entry("batch.size", 20),
                        MapEntry.entry("linger.ms", 1000),
                        MapEntry.entry("buffer.memory", 100),
                        MapEntry.entry("key.serializer", "key.serializer.value"),
                        MapEntry.entry("value.serializer", "value.serializer.value"));
    }
    @Test
    public void sendMessageToKafka() {
        // Prepare
        final ISimulatedDataGenerator simulatedDataGenerator =
                        mock(ISimulatedDataGenerator.class);
        final IJsonConverter jsonConverter = mock(IJsonConverter.class);
        final SimulatorJob sut = spy(new SimulatorJob(simulatedDataGenerator,
                        jsonConverter));
        final Properties props = mock(Properties.class);
        final String targetTopic = "targetTopic";
        final String msg = "msg";
        final Producer<String, String> producer = mock(Producer.class);
        doReturn(producer).when(sut).createKafkaProducer(props);
        final ProducerRecord<String,String> rec = mock(ProducerRecord.class);
        doReturn(rec).when(sut).createProducerRecord(targetTopic, msg);
        
        // Run method under test
        sut.sendMessageToKafka(props, targetTopic, msg);
        
        // Verify
        verify(sut).createKafkaProducer(props);
        verify(sut).createProducerRecord(targetTopic, msg);
        verify(producer).send(rec);
        verify(producer).close();
    }
}
