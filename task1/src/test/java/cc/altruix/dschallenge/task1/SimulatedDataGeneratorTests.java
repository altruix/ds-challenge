package cc.altruix.dschallenge.task1;

import java.time.LocalDateTime;
import java.util.Properties;
import java.util.Random;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.apache.commons.math3.random.RandomDataGenerator;

import static cc.altruix.dschallenge.task1.SimulatedDataGenerator.DEVICE_NR_BOUND;
import static cc.altruix.dschallenge.task1.SimulatedDataGenerator.LATITUDE;
import static cc.altruix.dschallenge.task1.SimulatedDataGenerator.MAX;
import static cc.altruix.dschallenge.task1.SimulatedDataGenerator.MAX_DEVICE_NR;
import static cc.altruix.dschallenge.task1.SimulatedDataGenerator.MIN;
import static cc.altruix.dschallenge.task1.SimulatedDataGenerator.MIN_DEVICE_NR;
import static cc.altruix.dschallenge.task1.SimulatedDataGenerator.TEMPERATURE;
import static cc.altruix.dschallenge.task1.Utils.toMillis;

public class SimulatedDataGeneratorTests {
    private static final double PRECISION = 1. / 1000000.;

    @Test
    public void generateDeviceNumber() {
        generateDeviceNumberTestLogic(0, MIN_DEVICE_NR);
        generateDeviceNumberTestLogic(DEVICE_NR_BOUND, MAX_DEVICE_NR);
    }

    @Test
    public void getDeviceUuid() {
        // Prepare
        final SimulatedDataGenerator sut = new SimulatedDataGenerator(
                        new Random(), new RandomDataGenerator());
        final Properties config = mock(Properties.class);
        final String propName = "dev1.id";
        final String propValue = "uuid1";
        when(config.getProperty(propName)).thenReturn(propValue);

        // Run method under test
        final String actRes = sut.getDeviceUuid(1, config);

        // Verify
        assertThat(actRes).isEqualTo(propValue);
        verify(config).getProperty(propName);
    }

    @Test
    public void generateTemperature() {
        generateTemperatureTestLogic(-10, 10, 0, -10);
        generateTemperatureTestLogic(-10, 10, 10 - (-10), 10);
    }

    @Test
    public void generatePositionalValue() {
        generatePositionalValueTestLogic(LATITUDE, -50.0, 40.0, 0, -50.0);
        generatePositionalValueTestLogic(LATITUDE, -50.0, 40.0, 1, 40.0);
    }

    private void generatePositionalValueTestLogic(final String propName,
                    final double min, final double max,
                    final double randomResult, final double expRes) {
        // Prepare
        final int devNr = 3;
        final Random random = mock(Random.class);
        when(random.nextDouble()).thenReturn(randomResult);
        final Properties config = mock(Properties.class);
        final SimulatedDataGenerator sut = spy(
                        new SimulatedDataGenerator(random, new RandomDataGenerator()));
        doReturn(min).when(sut).getDoubleBoundVal(devNr, propName, MIN, config);
        doReturn(max).when(sut).getDoubleBoundVal(devNr, propName, MAX, config);

        // Run method under test
        final double actRes = sut
                        .generatePositionalValue(devNr, config, propName);

        // Verify
        assertEquals(expRes, actRes, PRECISION);
    }

    private void generateTemperatureTestLogic(final int min, final int max,
                    final int randomResult, final int expRes) {
        // Prepare
        final int devNr = 2;
        final Properties config = mock(Properties.class);
        final Random random = mock(Random.class);
        when(random.nextInt(anyInt())).thenReturn(randomResult);
        final SimulatedDataGenerator sut = spy(
                        new SimulatedDataGenerator(random, new RandomDataGenerator()));
        doReturn(min).when(sut).getIntBoundVal(devNr, TEMPERATURE, MIN, config);
        doReturn(max).when(sut).getIntBoundVal(devNr, TEMPERATURE, MAX, config);

        // Run method under test
        final int actRes = sut.generateTemperature(devNr, config);

        // Verify
        assertThat(actRes).isEqualTo(expRes);
    }

    private void generateDeviceNumberTestLogic(final int randomValue,
                    final int expRes) {
        // Prepare
        final Random randomMock = mock(Random.class);
        when(randomMock.nextInt(anyInt())).thenReturn(randomValue);
        final SimulatedDataGenerator sut = new SimulatedDataGenerator(
                        randomMock, new RandomDataGenerator());

        // Run method under test
        final int actRes = sut.generateDeviceNumber();

        // Verify
        assertThat(actRes).isEqualTo(expRes);
    }


    @Test
    public void generateTimestamp() {
        final LocalDateTime minDate = LocalDateTime.of(2018, 9, 11, 0, 0);
        final LocalDateTime maxDate = LocalDateTime.of(2018, 9, 11, 23, 59);

        final long minDateMillis = toMillis(minDate);
        final long maxDateMillis = toMillis(maxDate);

        generateTimestampTestLogic("2018-09-11 00:00", "2018-09-11 23:59", minDateMillis,
                        minDateMillis);
        generateTimestampTestLogic("2018-09-11 00:00", "2018-09-11 23:59", maxDateMillis,
                        maxDateMillis);
        
    }

    private void generateTimestampTestLogic(final String minDateTime,
                    final String maxDateTime, final long randomNumber,
                    final long expRes) {
        // Prepare
        final int devNr = 3;
        final Properties config = mock(Properties.class);
        when(config.getProperty("dev3.time.min")).thenReturn(minDateTime);
        when(config.getProperty("dev3.time.max")).thenReturn(maxDateTime);

        final Random random = mock(Random.class);
        
        final RandomDataGenerator longRandom = 
                        mock(RandomDataGenerator.class);
        when(longRandom.nextLong(anyLong(), anyLong()))
                        .thenReturn(randomNumber);
        final SimulatedDataGenerator sut = spy(
                        new SimulatedDataGenerator(random, longRandom));

        // Run method under test
        final long actRes = sut.generateTimestamp(devNr, config);

        // Verify
        verify(longRandom).nextLong(anyLong(), anyLong());
        assertThat(actRes).isEqualTo(expRes);
    }
    @Test
    public void generateData() {
        // Prepare
        final SimulatedDataGenerator sut = spy(new SimulatedDataGenerator());
        final Properties cfg = mock(Properties.class);
        final int devNr = 1;
        doReturn(devNr).when(sut).generateDeviceNumber();
        final String devUuid = "UUID";
        doReturn(devUuid).when(sut).getDeviceUuid(devNr, cfg);
        final int temp = 30;
        doReturn(temp).when(sut).generateTemperature(devNr, cfg);
        final double lat = 10.;
        doReturn(lat).when(sut).generatePositionalValue(devNr, cfg, SimulatedDataGenerator.LATITUDE);
        final double lon = 20.;
        doReturn(lon).when(sut).generatePositionalValue(devNr, cfg, SimulatedDataGenerator.LONGITUDE);
        final long timestamp = 1L;
        doReturn(timestamp).when(sut).generateTimestamp(devNr, cfg);
        
        // Run method under test
        final SimulatedDataItem actRes = sut.generateData(cfg);

        // Verify
        assertThat(actRes).isNotNull();
        assertThat(actRes.getDeviceId()).isEqualTo(devUuid);
        assertThat(actRes.getLocation().getLatitude()).isEqualTo(lat);
        assertThat(actRes.getLocation().getLongitude()).isEqualTo(lon);
        assertThat(actRes.getTemperature()).isEqualTo(temp);
        assertThat(actRes.getTime()).isEqualTo(timestamp);
        
        verify(sut).generateDeviceNumber();
        verify(sut).getDeviceUuid(devNr, cfg);
        verify(sut).generateTemperature(devNr, cfg);
        verify(sut).generatePositionalValue(devNr, cfg, SimulatedDataGenerator.LATITUDE);
        verify(sut).generatePositionalValue(devNr, cfg, SimulatedDataGenerator.LONGITUDE);
        verify(sut).generateTimestamp(devNr, cfg);
    }
    @Test
    public void getIntBoundVal() {
        // Prepare
        final SimulatedDataGenerator sut = spy(new SimulatedDataGenerator());
        final Properties cfg = new Properties();
        cfg.setProperty("dev1.temperature.min", "-25");
        
        // Run method under test
        final int actRes = sut.getIntBoundVal(1, "temperature",
                        SimulatedDataGenerator.MIN, cfg);
        // Verify
        assertThat(actRes).isEqualTo(-25);
    }
    @Test
    public void getDoubleBoundVal() {
        // Prepare
        final SimulatedDataGenerator sut = spy(new SimulatedDataGenerator());
        final Properties cfg = new Properties();
        cfg.setProperty("dev3.lat.max", "41.24");

        // Run method under test
        final double actRes = sut
                        .getDoubleBoundVal(3, "lat", SimulatedDataGenerator.MAX,
                                        cfg);

        // Verify
        assertEquals(41.24, actRes, PRECISION);
    }
}
