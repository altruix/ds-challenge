package cc.altruix.dschallenge.task1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonConverter implements IJsonConverter {
    private static final ObjectMapper mapper = new ObjectMapper();
    private final static Logger LOGGER = 
                    LoggerFactory.getLogger(JsonConverter.class);
    @Override
    public String toJson(final SimulatedDataItem data) {
        try {
            return mapper.writeValueAsString(data);
        } catch (final JsonProcessingException exception) {
            LOGGER.error("Error while transforming object to JSON", 
                            exception);
            return "";
        }
    }
}
