package cc.altruix.dschallenge.task1;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;
import java.util.Random;
import java.util.function.Function;

import org.apache.commons.math3.random.RandomDataGenerator;

public class SimulatedDataGenerator implements ISimulatedDataGenerator {
    static final String TEMPERATURE = "temperature";
    static final String MAX = "max";
    static final String MIN = "min";
    static final String LATITUDE = "lat";
    static final String LONGITUDE = "lon";
    final static int MIN_DEVICE_NR = 1;
    final static int MAX_DEVICE_NR = ConfigValidator.DEVICE_COUNT;
    static final int DEVICE_NR_BOUND = MAX_DEVICE_NR - MIN_DEVICE_NR;
    private final static DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormatter
                    .ofPattern("yyyy-MM-dd HH:mm");
    private static SimulatedDataGenerator instance;
    private final Random random;
    private final RandomDataGenerator randomLongGen;

    static SimulatedDataGenerator getInstance() {
        if (instance == null) {
            instance = new SimulatedDataGenerator();
        }
        return instance;
    }

    SimulatedDataGenerator(final Random random,
                    final RandomDataGenerator randomLongGen) {
        this.random = random;
        this.randomLongGen = randomLongGen;
    }

    SimulatedDataGenerator() {
        this(new Random(), 
                        new RandomDataGenerator());
    }

    @Override
    public SimulatedDataItem generateData(final Properties config) {
        final int devNr = generateDeviceNumber();
        final String devUuid = getDeviceUuid(devNr, config);
        final int temp = generateTemperature(devNr, config);
        final double lat = generatePositionalValue(devNr, config, LATITUDE);
        final double lon = generatePositionalValue(devNr, config, LONGITUDE);
        final long timestamp = generateTimestamp(devNr, config);
        final Location loc = new Location(lat, lon);
        return new SimulatedDataItem(devUuid, temp, loc, timestamp);
    }

    long generateTimestamp(final int devNr, final Properties config) {
        final String devicePrefix = Utils.composeDevicePrefix(devNr);
        final String minPropName = String
                        .format("%stime.%s", devicePrefix, MIN);
        final String maxPropName = String
                        .format("%stime.%s", devicePrefix, MAX);
        final String minValStr = config.getProperty(minPropName);
        final String maxValStr = config.getProperty(maxPropName);

        final LocalDateTime minLdt = LocalDateTime
                        .parse(minValStr, DATE_TIME_FORMAT);
        final LocalDateTime maxLdt = LocalDateTime
                        .parse(maxValStr, DATE_TIME_FORMAT);
        final long minDateMillis = Utils.toMillis(minLdt);
        final long maxDateMillis = Utils.toMillis(maxLdt);
        return this.randomLongGen
                        .nextLong(minDateMillis, maxDateMillis);
    }

    double generatePositionalValue(final int devNr, final Properties config,
                    final String propName) {
        final double min = getDoubleBoundVal(devNr, propName, MIN, config);
        final double max = getDoubleBoundVal(devNr, propName, MAX, config);
        final double randomNumber = random.nextDouble();
        return (randomNumber * (max - min)) + min;
    }

    int generateTemperature(final int devNr, final Properties config) {
        final int lowerBound = getIntBoundVal(devNr, TEMPERATURE, MIN, config);
        final int upperBound = getIntBoundVal(devNr, TEMPERATURE, MAX, config);
        return this.random.nextInt(upperBound - lowerBound) + lowerBound;
    }

    private <T> T getBoundVal(final int devNr, final String prop,
                    final String valType, final Properties config,
                    final Function<String, T> converter) {
        final String devicePrefix = Utils.composeDevicePrefix(devNr);
        final String propName = String
                        .format("%s%s.%s", devicePrefix, prop, valType);
        final String propValue = config.getProperty(propName);
        return converter.apply(propValue);
    }

    int getIntBoundVal(final int devNr, final String prop, final String valType,
                    final Properties config) {
        return getBoundVal(devNr, prop, valType, config, Integer::valueOf);
    }

    double getDoubleBoundVal(final int devNr, final String prop,
                    final String valType, final Properties config) {
        return getBoundVal(devNr, prop, valType, config, Double::valueOf);
    }

    int generateDeviceNumber() {
        return random.nextInt(DEVICE_NR_BOUND) + 1;
    }

    String getDeviceUuid(final int devNr, final Properties config) {
        final String devicePrefix = Utils.composeDevicePrefix(devNr);
        final String propName = String.format("%sid", devicePrefix);
        return config.getProperty(propName);
    }
}
