package cc.altruix.dschallenge.task1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.lang3.tuple.Pair;

import cc.altruix.dschallenge.commons.AbstractValidator;

import static cc.altruix.dschallenge.task1.Utils.composeDevicePrefix;

public class ConfigValidator extends AbstractValidator {
    public final static int DEVICE_COUNT = 3;
    final static List<Pair<String,String>> MIN_MAX_PAIRS = Arrays.asList(
                    Pair.of("lat.min", "lat.max"),
                    Pair.of("lon.min", "lon.max"),
                    Pair.of("temperature.min", "temperature.max")
    );
    final static List<String> DEVICE_NON_EMPTY_PROPS = Arrays
                    .asList("id", "temperature.min", "temperature.max",
                                    "lat.min", "lat.max", "lon.min", "lon.max",
                                    "time.min", "time.max");
    private final static List<String> DEVICE_NUMERIC_PROPS = Arrays
                    .asList("temperature.min", "temperature.max", "lat.min",
                                    "lat.max", "lon.min", "lon.max");
    private final static List<String> DEVICE_DATE_TIME_PROPS = Arrays
                    .asList("time.min", "time.max");
    private final static List<String> NON_EMPTY_PROPS = Arrays
                    .asList("bootstrap.servers", "acks", "retries",
                                    "batch.size", "linger.ms", "buffer.memory",
                                    "key.serializer", "value.serializer",
                                    "targetTopic");
    private final static List<String> NUMERIC_PROPS = Arrays
                    .asList("retries", "batch.size", "linger.ms",
                                    "buffer.memory");
    private final List<String> nonEmptyProps;
    private final List<String> numericProps;
    private final List<String> dateTimeProps;
    private final List<Pair<String,String>> minMaxPairs;

    public ConfigValidator() {
        nonEmptyProps = createNonEmptyProps();
        numericProps = createNumericProps();
        dateTimeProps = createDateTimeProps();
        minMaxPairs = createMinMaxPairs();
    }
    protected List<Pair<String,String>> createMinMaxPairs() {
        return IntStream
                        .rangeClosed(1, DEVICE_COUNT).
                                        mapToObj(Utils::composeDevicePrefix)
                        .map(devicePrefix -> {
                            return MIN_MAX_PAIRS.stream().map(pair -> {
                                final String oldMin = pair.getLeft();
                                final String oldMax = pair.getRight();
                                
                                final String newMin = 
                                                prependDevPrefix(devicePrefix, oldMin);
                                final String newMax = 
                                                prependDevPrefix(devicePrefix,
                                                                oldMax);
                                return Pair.of(newMin, newMax);
                            }).collect(Collectors.toList());
                        }).flatMap(x -> x.stream())
                        .collect(Collectors.toList());
    }
    protected List<String> createDateTimeProps() {
        final List<String> dateTimeProps = new ArrayList<>(
                        DEVICE_DATE_TIME_PROPS.size() * DEVICE_COUNT);
        for (int i = 0; i < DEVICE_COUNT; i++) {
            final String devicePrefix = composeDevicePrefix(i + 1);
            DEVICE_DATE_TIME_PROPS.stream()
                            .map(prop -> prependDevPrefix(devicePrefix, prop))
                            .forEach(dateTimeProps::add);
        }
        return dateTimeProps;
    }

    protected List<String> createNumericProps() {
        final List<String> allNumericProps = new ArrayList<>(
                        DEVICE_NON_EMPTY_PROPS.size() * DEVICE_COUNT
                                        + NUMERIC_PROPS.size());
        for (int i = 0; i < DEVICE_COUNT; i++) {
            final String devicePrefix = composeDevicePrefix(i + 1);
            DEVICE_NUMERIC_PROPS.stream()
                            .map(prop -> prependDevPrefix(devicePrefix, prop))
                            .forEach(allNumericProps::add);
        }
        allNumericProps.addAll(NUMERIC_PROPS);
        return allNumericProps;
    }

    protected List<String> createNonEmptyProps() {
        final List<String> deviceSpecificNonEmptyProps = new ArrayList<>(
                        DEVICE_NON_EMPTY_PROPS.size() * DEVICE_COUNT);
        for (int i = 0; i < DEVICE_COUNT; i++) {
            final String devicePrefix = composeDevicePrefix(i + 1);
            DEVICE_NON_EMPTY_PROPS.stream()
                            .map(prop -> prependDevPrefix(devicePrefix, prop))
                            .forEach(deviceSpecificNonEmptyProps::add);
        }
        final List<String> res = new ArrayList<>(
                        deviceSpecificNonEmptyProps.size() + +NON_EMPTY_PROPS
                                        .size());
        res.addAll(deviceSpecificNonEmptyProps);
        res.addAll(NON_EMPTY_PROPS);
        return res;
    }

    private String prependDevPrefix(String devicePrefix, String prop) {
        return String.format("%s%s", devicePrefix, prop);
    }

    @Override
    protected List<String> getNonEmptyProps() {
        return nonEmptyProps;
    }

    @Override
    protected List<String> getNumericProps() {
        return numericProps;
    }

    @Override
    protected List<String> getDateTimeProps() {
        return dateTimeProps;
    }

    @Override
    protected List<Pair<String, String>> getMinMaxPairs() {
        return minMaxPairs;
    }
}
