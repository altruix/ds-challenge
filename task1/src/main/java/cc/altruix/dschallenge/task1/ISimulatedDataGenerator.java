package cc.altruix.dschallenge.task1;

import java.util.Properties;

public interface ISimulatedDataGenerator {
    SimulatedDataItem generateData(final Properties config);
}
