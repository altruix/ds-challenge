package cc.altruix.dschallenge.task1;

import java.util.Properties;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cc.altruix.dschallenge.commons.IConfigValidator;
import cc.altruix.dschallenge.commons.ValidationResult;

import cc.altruix.dschallenge.commons.Utils;

public class App {
    private final static String CONFIG_FILE_NAME = "task1.config.properties";
    private final IConfigValidator configValidator;
    private final Logger logger;

    public App(final IConfigValidator configValidator, final Logger logger) {
        this.configValidator = configValidator;
        this.logger = logger;
    }

    public App() {
        this(new ConfigValidator(), LoggerFactory.getLogger(App.class));
    }

    public void run() {
        final Properties cfg = readConfig();
        final ValidationResult valRes = configValidator.validate(cfg);
        if (!valRes.isValid()) {
            logger.error(String
                            .format("There is a problem with the configuration file: %s",
                                            valRes.getMessage()));
            return;
        }
        startScheduler(cfg);
    }

    Properties readConfig() {
        return Utils.readConfig(CONFIG_FILE_NAME, logger);
    }

    void startScheduler(final Properties cfg) {
        try {
            final Scheduler scheduler = createScheduler();
            final JobDetail job = createJobDetail(cfg);
            final Trigger trigger = createTrigger();
            scheduler.scheduleJob(job, trigger);
            scheduler.start();
        } catch (final SchedulerException e) {
            logger.error("Could not create scheduler", e);
        }
    }

    Trigger createTrigger() {
        return newTrigger()
                        .withIdentity("trigger1", "group1").startNow()
                        .withSchedule(simpleSchedule()
                                        .withIntervalInSeconds(1)
                                        .repeatForever()).build();
    }

    JobDetail createJobDetail(Properties cfg) {
        return newJob(SimulatorJob.class)
                        .withIdentity("job1", "group1")
                        .usingJobData(new JobDataMap(cfg)).build();
    }

    Scheduler createScheduler() throws SchedulerException {
        return StdSchedulerFactory
                        .getDefaultScheduler();
    }

    public static void main(final String[] args) {
        final App app = new App();
        app.run();
    }
}
