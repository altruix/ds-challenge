package cc.altruix.dschallenge.task1;

public interface IJsonConverter {
    String toJson(final SimulatedDataItem data);
}
