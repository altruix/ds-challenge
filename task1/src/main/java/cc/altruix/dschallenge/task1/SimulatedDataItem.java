package cc.altruix.dschallenge.task1;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

public class SimulatedDataItem {
    private final String deviceId;
    private final int temperature;
    private final Location location;
    private final long time;

    public SimulatedDataItem(
                    final String deviceId, 
                    final int temperature, 
                    final Location location, 
                    final long time) {
        this.deviceId = deviceId;
        this.temperature = temperature;
        this.location = location;
        this.time = time;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public int getTemperature() {
        return temperature;
    }

    public Location getLocation() {
        return location;
    }

    @JsonFormat(shape = Shape.STRING)
    public long getTime() {
        return time;
    }
}
