package cc.altruix.dschallenge.task1;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public final class Utils {
    private Utils() {
        
    }
    public static String composeDevicePrefix(final int devNr) {
        return String.format("dev%d.", devNr);
    }
    public static long toMillis(final LocalDateTime ldt) {
        final ZonedDateTime zdt = ldt.atZone(ZoneId.of("UTC"));
        return zdt.toInstant().toEpochMilli();
    }

}
