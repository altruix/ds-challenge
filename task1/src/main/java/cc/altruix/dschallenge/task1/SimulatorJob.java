package cc.altruix.dschallenge.task1;

import java.util.Properties;
import java.util.UUID;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimulatorJob implements Job {
    private final static Logger LOGGER = 
                    LoggerFactory.getLogger(SimulatorJob.class);
    public static final String TARGET_TOPIC = "targetTopic";
    private final ISimulatedDataGenerator simulatedDataGenerator;
    private final IJsonConverter jsonConverter;
    
    public SimulatorJob() {
        this(SimulatedDataGenerator.getInstance(),
                        new JsonConverter());
    }
    protected SimulatorJob(
                    final ISimulatedDataGenerator simulatedDataGenerator,
                    final IJsonConverter jsonConverter) {
        this.simulatedDataGenerator = simulatedDataGenerator;
        this.jsonConverter = jsonConverter;
    }

    @Override
    public void execute(final JobExecutionContext ctx) {
        final JobDataMap jdm = ctx.getJobDetail().getJobDataMap();
        final Properties kafkaProps = transformProps(jdm);
        final String targetTopic = jdm.getString(TARGET_TOPIC);
        final Properties config = extractConfig(jdm);
        final SimulatedDataItem simulatedDataItem = simulatedDataGenerator
                        .generateData(config);
        final String json = jsonConverter.toJson(simulatedDataItem);
        sendMessageToKafka(kafkaProps, targetTopic, json);
    }

    Properties extractConfig(final JobDataMap jdm) {
        final Properties config = new Properties();
        jdm.getWrappedMap().entrySet().stream()
                        .filter(x -> x.getValue() instanceof  String)
                        .forEach(x -> {
                            config.setProperty(x.getKey(), (String)x.getValue());
                        });
        return config;
    }

    Properties transformProps(final JobDataMap src) {
        final Properties props = new Properties();
        props.put("bootstrap.servers", 
                        src.getString("bootstrap.servers"));
        props.put("acks", src.getString("acks"));
        props.put("retries", src.getIntValue("retries"));
        props.put("batch.size", src.getIntValue("batch.size")
        );
        props.put("linger.ms", src.getIntValue("linger.ms"));
        props.put("buffer.memory", src.getIntValue("buffer.memory"));
        props.put("key.serializer",
                        src.getString("key.serializer")
        );
        props.put("value.serializer",
                        src.getString("value.serializer")
        );
        return props;
    }

    void sendMessageToKafka(final Properties props,
                    final String targetTopic, 
                    final String msg) {
        final Producer<String, String> producer = 
                        createKafkaProducer(props);
        final ProducerRecord<String,String> rec = 
                        createProducerRecord(targetTopic, msg);
        producer.send(rec);
        producer.close();
    }

    ProducerRecord<String, String> createProducerRecord(
                    final String targetTopic,
                    final String msg) {
        return new ProducerRecord<>(
                        targetTopic, UUID
                        .randomUUID().toString(), msg
        );
    }

    Producer<String, String> createKafkaProducer(final Properties props) {
        return new KafkaProducer<>(props);
    }
}
