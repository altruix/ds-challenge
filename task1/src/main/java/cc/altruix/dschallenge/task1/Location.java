package cc.altruix.dschallenge.task1;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

public class Location {
    private final double latitude;
    private final double longitude;

    public Location(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
    
    @JsonFormat(shape = Shape.STRING)
    public double getLatitude() {
        return latitude;
    }

    @JsonFormat(shape = Shape.STRING)
    public double getLongitude() {
        return longitude;
    }
}
