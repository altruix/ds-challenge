# Data Science Challenge

## Launch sequence

1. Start zookeeper (`start-zookeeper.bat`)
2. Start Kafka (`start-kafka.bat`)
3. Make sure the Kafka topic `task1` exists (if not, run `create-topic.bat`)

## How to list all topics

* `kafka-topics.bat`

## How to see all messages in a topic

* `consume-topic-task1.bat`

## Source

* [Quick start](https://kafka.apache.org/quickstart)
